extends Node2D

#TODO: udělat z toho button like switch

export (int, "Normal", "Switch", "Switch-single") var switch_mode
var active = false

signal switch_active
signal switch_inactive
signal switch_changed(status)

func _ready():
	# warning-ignore:return_value_discarded
	self.connect("switch_changed", self, "_switch_state_changed")
#	set_physics_process(false)
	add_to_group("teslatable")
	modulate = Color("ff0000")

func _switch_state_changed(state):
	if state:
		emit_signal("switch_active")
	else:
		emit_signal("switch_inactive")
	print(str(self) + " Switch changed state to " + str(state))

func set_active(set):
	#placeholder do doby, než bude grafika (nějaký jiskry, nebo "blesky" mezi kloboukem a switchem)
	if set:
		#Tesla effect on
		modulate = Color("00ff00")
	else:
		#Tesla effect off
		modulate = Color("ff0000")

func _end_tesla_stuff():
#	print(str(self) + " The tesla time is over!")
	if switch_mode == 0:
		active = false
		emit_signal("switch_changed", active)

func do_tesla_stuff():
#	print(str(self) + " Look mom! I am doing tesla stuff!")
	match switch_mode:
		0:
			active = true
			emit_signal("switch_changed", active)
		1:
			active = !active
			emit_signal("switch_changed", active)
		2:
			if not active:
				active = true
				emit_signal("switch_changed", active)
#
#
#func _physics_process(_delta):
#	if not active:
#		emit_signal("switch_inactive")
