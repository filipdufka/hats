extends AnimatedSprite

# where are the conventions ffs?
export (bool) var fireOn
export (bool) var isTimed
export (bool) var lightVisible

export (int) var flameDuration = 0
 

onready var HitboxAnimator = get_node("FireHitboxAnim")
# WTF is this?
# Private properties start witn _ and not end with it
onready var Light_ = get_node("Light2D")
onready var Shape_ = get_node("HurtArea/Shape")
var currentAnim = ""

#var _animation_index = 0
#var _animation_queue = ["starting", "default", "stopping", "off"]
var _next_animation = "starting"


#Var logic

func _ready():
	lightVisibility()
	#I could not 
	if fireOn:
		if isTimed and flameDuration > 0:
			$Timer.wait_time = flameDuration
			$Timer.start()
			_play_next()
		else:
			default()
	else:
		off()

#Player Detection + reloading lvl // Add fade in / out

func _on_HurtArea_body_entered(body) -> void:
	if body == Globals.player_ref:
		# warning-ignore:return_value_discarded
		GameOverScreen._fade_in(1)
		body.die()

#Turns the fire off

func off() -> void:
	Shape_.disabled = true
	HitboxAnimator.play("off")
	play("off")

#Fire on 24/7

func default() -> void:
	Shape_.disabled = false
	HitboxAnimator.play("default")
	play("default")


# This is garbage. First of all it is recursive, so it will stackoverflow and crash.
# Also it uses yields unsafely, so it raises error because it can't find the function after freeing the instance
# I fixed it fast and dirty, because I just don't care anymore

#Fire "ticks" (swtiches between on and off) every X seconds
#func tick(time) -> void:
#	pass
#	play("starting")
#	HitboxAnimator.play("FlameStarting")
#	yield(self, "animation_finished")
#	Shape_.disabled = false
#
#	play("default")
#	HitboxAnimator.play("FlameIdle")
#	yield(get_tree().create_timer(time), "timeout")
#	Shape_.disabled = true
#
#	play("stopping")
#	HitboxAnimator.play("FlameDying")
#	yield(self, "animation_finished")
#
#
#	play("off")
#	HitboxAnimator.play("FlameDead")
#	yield(get_tree().create_timer(time), "timeout")
#
#
#	tick(time)

func lightVisibility() -> void:
	Light_.visible = lightVisible

# Dafuq?
func scaleFire() -> void:
	pass


func _play_next():
	play(_next_animation)
	HitboxAnimator.play(_next_animation)
	match _next_animation:
		"starting":
			_next_animation = "default"
		"default":
			if $Timer.is_stopped():
				_next_animation = "stopping"
				$Timer.start()
		"stopping":
			_next_animation = "off"
		"off":
			if $Timer.is_stopped():
				_next_animation = "starting"
				$Timer.start()

func _on_BasicFire_animation_finished():
	if isTimed:
		_play_next()
