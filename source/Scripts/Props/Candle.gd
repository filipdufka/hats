extends Light2D
tool

export(int, 0, 5) var candle = 0 setget set_candle

onready var self_illuminator = $SelfIlluminator
onready var rng = RandomNumberGenerator.new()


func set_candle(can):
	candle = can
	$Candle.animation = "candle" + str(can)

func _ready():
	rng.randomize()
	set_candle(candle)

func _on_frame_changed():
	var rnd = rng.randf_range(0.6, 1)
	energy = rnd*1.2
	self_illuminator.energy = rnd
