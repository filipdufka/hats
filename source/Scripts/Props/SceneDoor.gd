extends Node2D
tool

export(String, FILE, "*.tscn,*.scn") var scene_to_load setget set_scene_to_load
enum door{BASIC, SLUMS, HEAVEN, DOWNTOWN, REDSKY, INVISIBLE}
export(door) var door_type setget set_door_type

var in_range = false


func _ready():
	if scene_to_load == "":
		push_error("Dane vyber scenu, co se nacte kdyz vlezes do dveri")
	$AnimatedSprite.play(str(door_type) + "-idle")

func set_door_type(type: int):
	door_type = type
	$AnimatedSprite.play(str(door_type) + "-idle")

func set_scene_to_load(value):
	if typeof(value) == TYPE_STRING and value.get_extension() in ["tscn", "scn"]:
		if Directory.new().file_exists(value):
			scene_to_load = value

func _on_AreaTrigger_trigger_entered() -> void:
	$PromptBody/Sprite/Prompt.text = InputMap.get_action_list("interact")[0].as_text()
	$PromptBody.show()
	in_range = true

func _on_AreaTrigger_trigger_exited() -> void:
	$PromptBody.hide()
	in_range = false

func _input(event):
	if not in_range: 
		return
	if event.is_action_pressed("interact"):
		changeScene()

func changeScene() -> void:
	if scene_to_load == "":
		push_error("Dane vyber scenu, co se nacte kdyz vlezes do dveri")
	else:
		$AnimatedSprite.play(str(door_type) + "-open")
		yield($AnimatedSprite, "animation_finished")
		SceneChanger.change_scene(scene_to_load)
