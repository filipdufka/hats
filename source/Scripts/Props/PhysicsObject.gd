extends RigidBody2D

export (int, "non-magnetic", "magnetic", "magnet") var magnet_mode = 0
export (float) var magnet_strength = 1

var _time_stopped = false
var _last_linear_velocity = Vector2(0,0)
var _last_angular_velocity = 0


func _ready():
	add_to_group("Objects")
	recalculate_settings()

func tesla_effect(on = true):
	if on:
		self.modulate = Color("ff0000")
	else:
		self.modulate = Color("ffffff")

func recalculate_settings():
	add_to_group("timeStoppable")
	
	if magnet_mode == 0:
		magnet_strength = 1
		if self.is_in_group("magnetable"):
			self.remove_from_group("magnetable")
	
	elif magnet_mode == 1:
		magnet_strength = 1
		add_to_group("magnetable")
	
	elif magnet_mode == 2:
		add_to_group("magnetable")
		if magnet_strength == 0:
			magnet_strength = 1
	
	else:
		push_error("Zadej validní hodnotu Dane")

func set_magnet_mode(mode):
	magnet_mode = mode
	recalculate_settings()


func _physics_process(_delta):
	if not _time_stopped:
		if magnet_mode == 2:
			for magnetable in get_tree().get_nodes_in_group("magnetable"):
				if magnetable != self:
					var direction = (get_global_position() - magnetable.get_global_position()).normalized()
					var force = direction * ((Globals.magnet_C * magnet_strength * magnetable.magnet_strength) / (pow(get_global_position().distance_to(magnetable.get_global_position()),2)))
					if not Globals.is_negligible(force.length()):
						magnetable.apply_central_impulse(force)
	else:
		pass


func time_stopped(time_stopped):
	_time_stopped = time_stopped
	if _time_stopped:
		$Sprite.global_position = global_position
		mode = RigidBody2D.MODE_STATIC
		_last_linear_velocity = linear_velocity
		_last_angular_velocity = angular_velocity
	else:
		linear_velocity = _last_linear_velocity
		angular_velocity = _last_angular_velocity
		mode = RigidBody2D.MODE_RIGID
