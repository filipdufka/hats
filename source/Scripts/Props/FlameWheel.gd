extends Position2D


onready var RotateTween = get_node("RotateTween")
export (int) var fullRotationTime
 

func _ready():
	rotate(fullRotationTime)



func rotate(duration) -> void:
	RotateTween.interpolate_property(self, "rotation_degrees", self.rotation_degrees, self.rotation_degrees + 360, duration, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	RotateTween.start()
	yield(RotateTween, "tween_completed")
	rotate(duration)
