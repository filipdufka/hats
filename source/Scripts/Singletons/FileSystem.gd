extends Node

#SET TO TRUE WHEN RELEASING
const paste_files = true
#SET TO TRUE WHEN RELEASING
const use_save_pass = false #Encrypts the save file if true. Can be turned off for debugging
const save_pass = "password1234"

var max_save_count = 5 # The maximum number of save files - give player the ability to change max save count?
const save_extension = ".save"
const save_directory = "saves"
const screenshot_directory = "screenshots"

func _init():
#	_txt("HATS_readme.txt", "HATS data and configuration can be found in: " + str(OS.get_user_data_dir()))
	_paste_files_to_paste()
	_init_storage()

# Helper: Initialize save storage for usage
# RECOMMENDATION: Call every time before working with saves!
func _init_storage():
	_create_dir(save_directory)
	_create_dir(screenshot_directory)


func _create_dir(dir_name):
	dir_name = "user://"+dir_name+"/"
	var status = AppData.create_directory(dir_name)
	if status != 0 and status != 32:
		push_error(dir_name + " init faiure: " + str(status))


func _txt(filename, content):
	var file = File.new()
	var err = file.open(OS.get_executable_path().get_base_dir()+"/"+filename, file.WRITE)
	if err == OK:
		file.store_string(content)
	else:
		push_error("Can't write .txt " + str(err))
	file.close()

# This is such a dirty solution, but I just don't care anymore
func _paste_files_to_paste():
	if paste_files:
		for f in _get_dir_contents("res://addons/Files_to_paste/"):
			_paste_file(f)


func _paste_file(file_name):
	var f = File.new()
	var to_write
	f.open("res://addons/Files_to_paste/" + file_name, File.READ)
	
	match file_name.get_extension():
		"all": #Just paste
			var r = RegEx.new()
			r.compile("\\$user://")
			to_write = r.sub(f.get_as_text(), str(OS.get_user_data_dir()), true)
			f.close()
			f.open(OS.get_executable_path().get_base_dir()+"/"+file_name.get_basename(), File.WRITE)
			f.store_string(to_write)
			f.close()
			return
		"win": #Just paste but only on Windows
			if OS.get_name() == "Windows":
				to_write = f.get_buffer(f.get_len())
			else:
				return
		"lnx": #Just paste but only on Linux
			if OS.get_name() == "X11":
				to_write = f.get_buffer(f.get_len())
			else:
				return
		"mac": #Just paste but only on Mac
			if OS.get_name() == "OSX":
				to_write = f.get_buffer(f.get_len())
			else:
				return
		_:
			return
	
	f.close()
	f.open(OS.get_executable_path().get_base_dir()+"/"+file_name.get_basename(), File.WRITE)
	f.store_buffer(to_write)
	f.close()


func _get_dir_contents(dir_name: String):
	var dir = Directory.new()
	dir.open(dir_name)
	if dir.list_dir_begin(true) == OK:
		var content = []
		while true:
			var file = dir.get_next()
			if file != "":
				content.append(file)
			else:
				break
				
		dir.list_dir_end()
		return content
	return null


func create_save(scene_to_be_loaded: String, scene_args: Array=[]):
	#Save creation
	var save_data = get_blank_save()
	var file = File.new()
	if file.file_exists(scene_to_be_loaded): #Check for correct input
		save_data["meta"]["UnixTimeStamp"] = OS.get_unix_time()
		save_data["playerData"]["activeHat"] = GameState.data["hat"]["active"]
		save_data["playerData"]["acquiredHats"] = GameState.data["acquiredHats"]
		save_data["worldData"]["activeScene"]["scene"] = scene_to_be_loaded
		save_data["worldData"]["activeScene"]["args"] = scene_args
		
		save_data["clydeData"] = get_clyde_data_from_last_save()
		for clyde in get_tree().get_nodes_in_group("Clyde"):
			var clyde_to_save = clyde._dialogue.get_save_data_buffer()
			save_data["clydeData"][clyde_to_save["dialogue"]] = clyde_to_save["dialogueData"]
	else:
		push_error("Invalid scene_to_be_loaded by save creator")
		save_data = get_blank_save()
	file.close()
	
	#Save saving
	var err
	if use_save_pass:
		err = file.open_encrypted_with_pass("user://"+save_directory + "/" + str(save_data["meta"]["UnixTimeStamp"]) + save_extension, File.WRITE, save_pass)
	else:
		err = file.open("user://"+save_directory + "/" + str(save_data["meta"]["UnixTimeStamp"]) + save_extension, File.WRITE)
	
	file.store_string(JSON.print(save_data, "\t"))
	file.close()
	if not err:
		print("Save '" + str(save_data["meta"]["UnixTimeStamp"]) + ".save' saved")
		#delete excess files
		var all_saves = sort_save_names(get_save_names())
		if all_saves.size() > max_save_count:
			for i in range(max_save_count, all_saves.size()):
				delete_save(all_saves[i])
	else:
		push_error("Failed to write save. err (" + str(err) + ")")
	return err


#TODO - check the save and load if possible
func load_save(save_name):
	if save_name in get_save_names():
		var save_data = get_save_as_dict(save_name)
		if save_data:
			delete_saves_newer_than(save_name)
			load_data_to_memory(save_data)
		else:
			return 2 #invalid save data (parse error?)
	else:
		return 1 #save does not exist


func get_save_names():
	_init_storage()
	var dir_content = AppData.directory_content(save_directory)
	var save_names = []
	for file in dir_content:
		if AppData.is_file(save_directory+"/"+file):
			#thx hrle
			var regex = RegEx.new()
			regex.compile("^\\d+."+save_extension+"$")
			var result = regex.search(file)
			if result and result.get_string() == file:
				save_names.append(file)
	return save_names


#index 0 - newest save
func sort_save_names(saves):
	for i in range(saves.size()):
		saves[i] = saves[i].substr(0, saves[i].length()-save_extension.length())
	saves.sort()
	saves.invert()
	for i in range(saves.size()):
		saves[i] = saves[i]+save_extension
	return saves


#gets save file contents as dict - assumes the file exists
func get_save_as_dict(save_name):
	_init_storage()
	var file = File.new()
	var result
	if use_save_pass: 
		result = file.open_encrypted_with_pass("user://"+save_directory+"/"+save_name, File.READ, save_pass)
	else:
		result = file.open("user://"+save_directory+"/"+save_name, File.READ)
		
	if not result:
		result = file.get_as_text()
		result = JSON.parse(result)
		if not result.error:
			return result.result
		else:
			push_error("Save parse error")
	else:
		push_error("Can't open save")
	return {}


#loads all saved data to gamestate - assumes that they are safe
func load_data_to_memory(saved_data):
	#load saved clyde data to be ready when requested
	for key in saved_data["clydeData"].keys():
		GameState.clyde_data[key] = saved_data["clydeData"][key]
	#indirect hat setting
	GameState.unlock_hat("NullHat")
	GameState.unlock_hat(saved_data["playerData"]["activeHat"])
	GameState.data["hat"]["active"] = saved_data["playerData"]["activeHat"]
	for hat in saved_data["playerData"]["acquiredHats"]:
		GameState.unlock_hat(hat)
	#load correct scene
	SceneChanger.change_scene(saved_data["worldData"]["activeScene"]["scene"])


func delete_saves_newer_than(save_name):
	_init_storage()
	if AppData.file_exists(save_directory+"/"+save_name):
		var saves = sort_save_names(get_save_names())
		while saves[0] != save_name:
			delete_save(saves[0])
			saves.remove(0)
	else:
		printerr("Save '"+ save_name +"' does not exist so save newer saves cannot be deleted")


func delete_save(save_name):
	_init_storage()
	if AppData.file_exists(save_directory+"/"+save_name):
		AppData.delete(save_directory+"/"+save_name)
		print("Save '" + save_name + "' deleted")
	else:
		printerr("Save '"+ save_name +"' does not exist")


func get_clyde_data_from_last_save():
	var all_saves = sort_save_names(get_save_names())
	if all_saves:
		var save = get_save_as_dict(all_saves[0])
		return save["clydeData"] if save.has("clydeData") else {}
	else:
		return {}


func get_blank_save():
	return {
		"meta": {
			"UnixTimeStamp": 0, 
			},
		"playerData": {
			"activeHat": "NullHat",
			"acquiredHats": ["NullHat"]
			},
		"clydeData": {
			},
		"worldData": {
			"activeScene": {"scene":"res://Scenes/UI/Screens/Init.tscn", "args":[]}
			}
		}
