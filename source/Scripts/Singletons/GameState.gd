#Copyright (c) 2021 Adam Charvát & Jakub Janšta 
#  - GameState system 
#  - Manages game state
#  - Introduces the use of nested object paths
#  - For example 'user.age' or 'user/age' or 'user_age' ...
#
# - Save system
#  - Write current GameState data to disk and later load it back into system memory
#  - Saves located at user://saves by default

extends Node

# ---[ GAME STATE SYSTEM ]---

#Cache for clyde dialogues. Clyde can access this data at any time, so don't modify this
var clyde_data = {}

# State registry
var data = {
	"player": { "canMove": true },
	"hat": {
		"active": "NullHat",
		"isEnabled": true,
	},
	#DO NOT MODIFY - this should be handled by save system
	#For debug purposes you can temporarily change this, but revert the change afterward
	"acquiredHats": [ "NullHat" ] #[ "MagneticHat", "TimeHat", "GenericHat", "TeslaHat", "NullHat", "DebugHat" ]
}

# Path separator used for differencing between multiple variable names
#	- WARNING: Path separator MUST be a character that you aren't using in the variable names otherwise it won't work
const path_separator = "."

# setv(path, value)
# - Set state registry value/variable
# @param path: nested object path (e.g. hat.isEnabled)
# @param value: new value to assign (e.g. true)
# If the variable does not exist, create it.
#
# Rewritten from https://github.com/Skulaurun/a-periment/blob/6690d556a783fa607c1a131680aed564ac320868/src/main/storage.js
func setv(path, value):
	if typeof(path) == TYPE_STRING:
		path = path.split(path_separator)
	var current_object = data
	for i in range(len(path) - 1):
		var part = path[i]
		if not part in current_object:
			current_object[part] = {}
		current_object = current_object[part]
	current_object[path[-1]] = value

# getv(path)
# - Get value from state registry
# @param path: nested object path (e.g. hat.isEnabled)
# @return value
# If the variable does not exist, returns null.
#
# Rewritten from https://github.com/Skulaurun/a-periment/blob/6690d556a783fa607c1a131680aed564ac320868/src/main/storage.js
func getv(path = null):
	if typeof(path) == TYPE_STRING:
		path = path.split(path_separator)
	else:
		return data
	var current_object = data
	for part in path:
		if typeof(current_object) != TYPE_DICTIONARY or not part in current_object:
			return null
		current_object = current_object[part]
	return current_object


func unlock_hat(hat):
	if Globals.is_hat_name_valid(hat):
		if not hat in data["acquiredHats"]:
			data["acquiredHats"].append(hat)
			print("Hat '" + hat + "' unlocked")
		return 0
	else:
		printerr("Hat '" + hat + "' does not exist (unlock)")
		return 1


func lock_hat(hat):
	if Globals.is_hat_name_valid(hat):
		if hat != "NullHat":
			if hat in data["acquiredHats"]:
				data["acquiredHats"].erase(hat)
				print("Hat '" + hat + "' locked")
		return 0
	else:
		printerr("Hat '" + hat + "' does not exist (lock)")
		return 1

#Scene_to_be_loaded - path to scene that will be loaded when the save is loaded (res://Scenes/Levels/...)
#For instance the door saves the game and as the argument passes the scene that will be loaded behind the door 
