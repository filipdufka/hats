extends CanvasLayer



var DeathRegistered = false
onready var TransTween = get_node("TransitionTween")

onready var Rect = get_node("Control/ColorRect")
onready var SmartLabel = get_node("Control/SmartLabel")

onready var DeathHat = get_node("DeathHat")
onready var FadePlayer = get_node("FadePlayer")

var TextSet = false
var DeathCauseType

const FireTips = ["Even cavebots knew not to touch fire.",
"Toasted.",
"You can't toast a robot, but you are Lapse so...",
"You Died.",
"ITS ONLY GAME WHY U HAVE TO BE MAD."
]

const DoorTips = ["Being robot != immortal.",
"Door'd.",
"What?",
"You Died.",
"ITS ONLY GAME WHY U HAVE TO BE MAD."
]

const FallTips = ["Does the player seem like a plane to you?",
"Notworth fell down and kept falling.",
"At least you've made it that far :)",
"You Died.",
"Es un idiota, así que no lo vi, saltó.",
"ITS ONLY GAME WHY U HAVE TO BE MAD.",
"Now, Do, do, do, do, do. That's an idiot, so I didn't see it. Svarta, are you okay? Well, that's it! Stop it and do it! Shit, shit is nothing to him, man. He is very much!"]

func _ready():
	$Control.visible = false
	$DeathHat.visible = false

func _fade_in(cause) -> void:
	FadePlayer.play("FadeIn")
	DeathRegistered = true
	if DeathRegistered == true:
		setDeathTip(cause)


func _fade_out() -> void:
	FadePlayer.play("FadeOut")
	yield(FadePlayer, "animation_finished")
	
	SmartLabel.set_text("")
	DeathRegistered = false
	TextSet = false
	

func reloadScene() -> void:
	# warning-ignore:return_value_discarded
	get_tree().reload_current_scene()


func setDeathTip(cause) -> void:
	match cause:
		1:
			DeathCauseType = FireTips[randi() % FireTips.size()]
		2:
			DeathCauseType = DoorTips[randi() % DoorTips.size()]
		3:
			DeathCauseType = FallTips[randi() % FallTips.size()]
	
	if DeathCauseType != "":
		if TextSet == false:
			SmartLabel.set_text(DeathCauseType)
			DeathRegistered = false
			TextSet = true

