#Copyright (c) 2020-2021 Jakub Janšta
#
#Description:
#This script handles its own local filesystem in user://

extends Node


#read_file(file_name)
#Reads specified file in app's filesystem and returns it's contents
#
#@param file_name: name of file you wish to read
#
#@return array: index 0: error:  0  - OK
#                                1  - file not found
#                                2+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error
#               
#               index 1: result: null   - error
#                                string - valid result
#
func read_file(file_name):
	file_name = "user://" + file_name
	var file = File.new()
	if file.file_exists(file_name):
		var err = file.open(file_name, file.READ)
		if err == OK:
			var result = file.get_as_text()
			file.close()
			return [0, result]
		return [err, null]
	return [1, null]


#write_file(file_name, data)
#Writes or overwrites data to file in string format. New file is created if it does not exist
#
#@param file_name:  name of file you wish to write to
#@param data:       data you wish to write to file as string (will be converted to string if variable is not string)
#
#@return error:     0  - OK
#                   1+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error 
#
func write_file(file_name, data):
	file_name = "user://" + file_name
	var file = File.new()
	var err = file.open(file_name, file.WRITE)
	if err == OK:
		file.store_string(str(data))
	file.close()
	return err


#read_file_as_JSON(file_name)
#Reads specified file in app's filesystem and returns it's contents as dictionary
#
#@param file_name: name of file you wish to read
#
#@return array: index 0: error:  0  - OK
#                                1  - file not found
#                                2  - JSON parse error
#                                3+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error
#               
#               index 1: result: null   - error
#                                string - valid result
#
func read_file_as_JSON(file_name):
	var result = read_file(file_name)
	if result[0] == OK:
		var decoded = JSON.parse(result[1])
		if decoded.error == OK:
			return [0, decoded.result]
		return [2, null]
	return result


#write_file_as_JSON(file_name, data)
#Writes or overwrites data to file in JSON string format. New file is created if it does not exist
#
#@param file_name:  name of file you wish to write to
#@param data:       data you wish to write to file as dictionary
#
#@return error:     0  - OK
#                   1+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error 
#
func write_file_as_JSON(file_name, data):
	return write_file(file_name, JSON.print(data))


#file_exists(file_name)
#Checks if file with specified name exists in app's filesystem
#
#@param file_name: name of file you wish to check if it exists
#
#@return exists:   true:  file exists
#                  false: file does not exist
#
func file_exists(file_name):
	file_name = "user://" + file_name
	var file = File.new()
	file_name = file.file_exists(file_name)
	file.close()
	return file_name


#empty_file(file_name)
#Dumps existing files contents or creates empty file if it does not exist
#
#@param file_name: name of file you wish to dump or create
#
#@return contents: returns content of file that was erased. If the file was empty or was just created empty string is returned
#
func empty_file(file_name):
	file_name = "user://" + file_name
	var content = ""
	var file = File.new()
	if file.file_exists(file_name):
		var err = file.open(file_name, file.WRITE_READ)
		if err == OK:
			content = file.get_as_text()
	file.store_string("")
	file.close()
	return content


#directory_exists(dir_name)
#Checks if directory with specified name exists in app's filesystem
#
#@param dir_name: name of directory you wish to check if it exists
#
#@return exists:   true:  directory exists
#                  false: directory does not exist
#
func directory_exists(dir_name):
	dir_name = "user://" + dir_name
	var dir = Directory.new()
	return dir.dir_exists(dir_name)


#create_directory(dir_name)
#Creates a directory with specified name
#
#@param dir_name: name of directory you want to create
#
#@return error:   0  - OK
#                 1+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error
#
func create_directory(dir_name):
	var dir = Directory.new()
	return dir.make_dir(dir_name)


#directory_content(dir_name="")
#Returns array of strings of directory contents 
#
#@param dir_name: name of directory whom you want it's contents. If not specified root directory is used
#
#@return content: array of file and directory names as string
#
func directory_content(dir_name=""):
	dir_name = "user://" + dir_name
	var dir = Directory.new()
	
	dir.open(dir_name)
	if dir.list_dir_begin(true) == OK:
		var content = []
		while true:
			var file = dir.get_next()
			if file != "":
				content.append(file)
			else:
				break
				
		dir.list_dir_end()
		return content
	return null


#delete(name)
#Deletes the target file or an empty directory
#
#@param name: name of file or empty directory you want to delete
#
#@return error: 0  - OK
#               1+ - see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error
#
func delete(name):
	name = "user://" + name
	var dir = Directory.new()
	return dir.remove(name)

#true - is file
#false - is dir
func is_file(name):
	name = "user://" + name
	var f = File.new()
	name = f.file_exists(name)
	f.close()
	return name
