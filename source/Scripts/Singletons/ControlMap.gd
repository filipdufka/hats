# Copyright (c) 2021 Adam Charvát

extends Node

const prefix = "controls.keyboard."

func _ready():
	clear()
	load_from_config()

func load_from_config():
	for key in Config.find(prefix, false, true):
		if not InputMap.has_action(key):
			InputMap.add_action(key)
		if Config.getv(prefix+key):
			_bind_helper(key, Config.getv(prefix+key))

func _bind_helper(action, key, is_internal = true):
	var event = InputEventKey.new()
	event.scancode = key
	InputMap.action_add_event(action, event)
	if not is_internal:
		Config.setv(prefix + action, key)

func bind(action, key):
	clear([action])
	_bind_helper(action, key, false)
	
func unbind(action):
	clear([action])
	Config.setv(prefix + action, null)

func is_bound(action):
	return Config.getv(prefix + action) != null

func clear(actions = InputMap.get_actions()):
	for action in actions:
		InputMap.action_erase_events(action)
