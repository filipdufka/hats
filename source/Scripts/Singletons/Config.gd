# Copyright (c) 2021 Adam Charvát & Jakub Janšta

extends Node

const config_file = "config.json"
const config_file_max_size = 2048 #bytes


func _init():
	if not load_from_file():
		save_to_file()
		
	#do config stuff like setting the properties
	Globals.debug = data["game.debug"]
	#audio (audio buses use (-80;6) range instead of (0;100). Use Globals.remap_value_between_ranges() to convert between them)
	data["audio.bus.master.volume"] = clamp(data["audio.bus.master.volume"], 0, 100)
	data["audio.bus.effects.volume"] = clamp(data["audio.bus.effects.volume"], 0, 100)
	data["audio.bus.music.volume"] = clamp(data["audio.bus.music.volume"], 0, 100)
	data["audio.bus.ambient.volume"] = clamp(data["audio.bus.ambient.volume"], 0, 100)
	update_audio_buses_volume_from_config()
	
#	dump_to_log()

# DEFAULT VALUES
var data = {
	#game
	"game.debug": false,
	"game.window.mode": 0,
	"game.window.mode_debug": 1,
	"game.window.borderless.size_x": 1280,
	"game.window.borderless.size_y": 720,
	"game.window.borderless.position_x": 50,
	"game.window.borderless.position_y": 25,
	#Controls
	"controls.keyboard.move_left": KEY_A,
	"controls.keyboard.move_right": KEY_D,
	"controls.keyboard.move_up": KEY_W,
	"controls.keyboard.move_down": KEY_S,
	"controls.keyboard.move_jump": KEY_SPACE,
#	"controls.keyboard.look_up": KEY_W,
#	"controls.keyboard.look_down": KEY_A,
	"controls.keyboard.hat_action_1": KEY_Q,
	"controls.keyboard.hat_action_2": KEY_E,
	"controls.keyboard.interact": KEY_F,
	"controls.keyboard.dialogue_up": KEY_W,
	"controls.keyboard.dialogue_down": KEY_S,
	"controls.keyboard.dialogue_continue": KEY_SPACE,
	"controls.keyboard.dialogue_select": KEY_ENTER,
	"controls.keyboard.console_toggle": KEY_QUOTELEFT,
	"controls.keyboard.console_history_up": KEY_UP,
	"controls.keyboard.console_history_down": KEY_DOWN,
	#Audio
	"audio.bus.master.volume": 55,
	"audio.bus.effects.volume": 75,
	"audio.bus.music.volume": 75,
	"audio.bus.ambient.volume": 75,
}

func setv(key, value):
	data[key] = value
	save_to_file()

func getv(key):
	if not key:
		return data
	if exists(key):
		return data[key]
	else:
		return null

func exists(key):
	return key in data

func find(phrase, only_keys = true, strip = false):
	# warning-ignore:incompatible_ternary
	var matches = [] if only_keys else {}
	for key in data.keys():
		if key.begins_with(phrase):
			var stripped_key = key if not strip else key.replace(phrase, "")
			if only_keys:
				matches.append(stripped_key)
			else:
				matches[stripped_key] = data[key]
	return matches

func save_to_file():
	var json = JSON.print(data, "\t")
	AppData.write_file(config_file, json)

func load_from_file():
	var f = File.new()
	var err = f.open("user://"+config_file, File.READ)
	if not err:
		if f.get_len() < config_file_max_size:
			err = JSON.parse(f.get_as_text())
			if not err.error: 
				#load the config to data (the one big fkin dictionary in this file)
				for key in err.result.keys():
					data[key] = err.result[key]
				f.close()

				
				save_to_file()
				return true
			else:
				push_error("Can't parse config.json. err (" + str(err.error) + ") " + str(err.error_string))
		else:
			push_error("Maximal allowed config.json file size exceeded (" + str(f.get_len()) + "/" + str(config_file_max_size) + " bytes)")
	else:
		if err != 7: #File not found
			push_error("Can't open config.json. err " + str(err))
	
	f.close()
	return false

func dump_to_log():
	print("\nConfig:")
	for key in data.keys():
		print("\t" + key + ": " + str(data[key]))
	print("\n")

# These long statements give me Java flashbacks
func update_audio_buses_volume_from_config():
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"),  Globals.remap_value_between_ranges(data["audio.bus.master.volume"], Vector2(0,100), Vector2(-80,24)))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Effects"), Globals.remap_value_between_ranges(data["audio.bus.effects.volume"], Vector2(0,100), Vector2(-80,24)))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"),   Globals.remap_value_between_ranges(data["audio.bus.music.volume"], Vector2(0,100), Vector2(-80,24)))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Ambient"), Globals.remap_value_between_ranges(data["audio.bus.ambient.volume"], Vector2(0,100), Vector2(-80,24)))
