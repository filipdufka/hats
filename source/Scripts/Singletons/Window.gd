extends Node

# SD (Standard Definition)	480p	4:3	640 x 480
# HD (High Definition)	720p	16:9	1280 x 720
# Full HD (FHD)	1080p	16:9	1920 x 1080
# QHD (Quad HD)	1440p	16:9	2560 x 1440
# 2K video	1080p	1:1.77	2048 x 1080
# 4K video or Ultra HD (UHD)	4K or 2160p	1:1.9	3840 x 2160
# 8K video or Full Ultra HD	8K or 4320p	16∶9	7680 x 4320

var _load_dimmer = preload("res://Scenes/UI/Canvas/Load_dimmer.tscn").instance()

signal dim_finished
signal undim_finished

func _init():
	add_child(_load_dimmer)

func _ready():
	# warning-ignore:return_value_discarded
	OS.center_window()
	set_window_accept_quit(false)
	set_mode()

# -1 : User default from config
# 0  : Fullscreen
# 1  : Windowed
# 2  : Windowed Borderless 
func set_mode(mode: int=-1) -> void:
	match mode:
		-1: 
			if not Globals.debug:
				# warning-ignore:narrowing_conversion
				set_mode(clamp(int(Config.data["game.window.mode"]), 0, 2))
			else:
				# warning-ignore:narrowing_conversion
				set_mode(clamp(int(Config.data["game.window.mode_debug"]), 0, 2))
		0 : fullscreen()
		1 : windowed()
		2 : windowed_borderless()
		_ : push_error("Invalid window mode")

func fullscreen():
#	print("Window FULLSCREEN")
	set_window_fullscreen(true)
	set_window_borderless(false)
	set_window_resizable(false)
#	set_window_always_on_top(true)
	set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP)
	set_window_min_size(Vector2(640, 480))

func windowed():
#	print("Window WINDOWED")
	set_window_fullscreen(false)
	set_window_borderless(false)
	set_window_resizable(true)
#	set_window_always_on_top(false)
	set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP)
	set_window_min_size(Vector2(640, 480))

func windowed_borderless():
	#correct size from windowed (delete black bars)
#	print("Window WINDOWED_BORDERLESS")
	set_window_size(Vector2(Config.data["game.window.borderless.size_x"], Config.data["game.window.borderless.size_y"]))
	set_window_position(Vector2(Config.data["game.window.borderless.position_x"],Config.data["game.window.borderless.position_y"]))
	set_window_fullscreen(false)
	set_window_borderless(true)
	set_window_resizable(true)
#	set_window_always_on_top(true) #nastaveno na true opraví překrytí lištou
	set_screen_stretch(SceneTree.STRETCH_MODE_2D, SceneTree.STRETCH_ASPECT_KEEP)
	set_window_min_size(Vector2(640, 480))
	

# MOUSE_MODE_VISIBLE  : 0: Makes the mouse cursor visible if it is hidden.
# MOUSE_MODE_HIDDEN   : 1: Makes the mouse cursor hidden if it is visible.
# MOUSE_MODE_CAPTURED : 2: Captures the mouse. The mouse will be hidden and its position locked at the center of the screen.
# MOUSE_MODE_CONFINED : 3: Makes the mouse cursor visible but confines it to the game window.
func set_mouse_mode(mode: int) -> void:
	Input.set_mouse_mode(mode if mode>-1 and mode<4 else 0)

func get_mouse_mode() -> int:
	return get_mouse_mode()

# Window stuff
func set_window_position(position: Vector2) -> void:
	OS.set_window_position(position)

func get_window_position() -> Vector2:
	return OS.get_window_position()

func set_window_size(size: Vector2) -> void:
	OS.set_window_size(size)

func get_window_size() -> Vector2:
	return OS.get_window_size()

func get_real_window_size() -> Vector2:
	return OS.get_real_window_size()

func set_window_fullscreen(mode: bool) -> void:
	OS.set_window_fullscreen(mode)

func get_window_fullscreen() -> bool:
	return OS.is_window_fullscreen()

func set_window_resizable(mode: bool) -> void:
	OS.set_window_resizable(mode)

func get_window_resizable() -> bool:
	return OS.is_window_resizable()

func set_window_minimized(mode: bool) -> void:
	OS.set_window_minimized(mode)

func get_window_minimized() -> bool:
	return OS.is_window_minimized()

func set_window_maximized(mode: bool) -> void:
	OS.set_window_maximized(mode)

func get_window_maximized() -> bool:
	return OS.is_window_maximized()

func set_window_accept_quit(mode: bool) -> void:
	get_tree().set_auto_accept_quit(mode)

func set_window_vsync(mode: bool) -> void:
	OS.set_use_vsync(mode)

func get_window_vsync() -> bool:
	return OS.is_vsync_enabled()

func set_window_always_on_top(mode: bool) -> void:
	OS.set_window_always_on_top(mode)

func get_window_always_on_top() -> bool:
	return OS.is_window_always_on_top()

func set_window_min_size(size: Vector2=Vector2(0,0)) -> void:
	OS.set_min_window_size(size)

func get_window_min_size() -> Vector2:
	return OS.get_min_window_size()

func set_window_borderless(mode: bool) -> void:
	OS.set_borderless_window(mode)

func get_window_borderless() -> bool:
	return OS.get_borderless_window()

func get_screen_count() -> int:
	return OS.get_screen_count()

# warning-ignore:unused_argument
func get_screen_dpi(screen: int=-1) -> int:
	return OS.get_screen_dpi(screen)

func get_screen_max_scale() -> float:
	return OS.get_screen_max_scale()

func get_screen_position(screen: int=-1) -> Vector2:
	return OS.get_screen_position(screen)

func get_screen_scale(screen: int=-1) -> float:
	return OS.get_screen_scale(screen)

func get_screen_size(screen: int=-1) -> Vector2:
	return OS.get_screen_size(screen)

# STRETCH_MODE_DISABLED = 0 --- No stretching.
# STRETCH_MODE_2D = 1 --- Render stretching in higher resolution (interpolated).
# STRETCH_MODE_VIEWPORT = 2 --- Keep the specified display resolution. No interpolation. Content may appear pixelated.
#
# STRETCH_ASPECT_IGNORE = 0 --- Fill the window with the content stretched to cover excessive space. Content may appear stretched.
# STRETCH_ASPECT_KEEP = 1 --- Retain the same aspect ratio by padding with black bars on either axis. This prevents distortion.
# STRETCH_ASPECT_KEEP_WIDTH = 2 --- Expand vertically. Left/right black bars may appear if the window is too wide.
# STRETCH_ASPECT_KEEP_HEIGHT = 3 --- Expand horizontally. Top/bottom black bars may appear if the window is too tall.
# STRETCH_ASPECT_EXPAND = 4 --- Expand in both directions, retaining the same aspect ratio. This prevents distortion while avoiding black bars.
func set_screen_stretch(stretch_mode: int=0, aspect: int=0, minsize: Vector2 = Vector2(1280,720), shrink: float=1):
	get_tree().set_screen_stretch(stretch_mode, aspect, minsize, shrink)

func get_screenshot():
	var image = get_viewport().get_texture().get_data()
	image.flip_y()
	return image

func save_screenshot(screenshot: Image):
	return screenshot.save_png("user://"+FileSystem.screenshot_directory+"/HATS_screenshot-"+str(OS.get_unix_time())+".png")

func screenshot():
	save_screenshot(get_screenshot())
	dim_flash()
	yield(get_tree().create_timer(.2), "timeout")
	CustomPopup.create_simple("Screenshot saved", "hat", 1.5)

func _input(event):
	if event is InputEventKey and event.is_pressed() and not event.is_echo() and event.scancode == KEY_F6:
		screenshot()

func dim():
	_load_dimmer.get_node("AnimationPlayer").play("dim")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("dim_finished")
	
func undim():
	_load_dimmer.get_node("AnimationPlayer").play_backwards("dim")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("undim_finished")
	
func dim_loading():
	_load_dimmer.randon_lst()
	_load_dimmer.get_node("Loading").visible = true
	_load_dimmer.get_node("AnimationPlayer").play("dim_loading")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("dim_finished")
	
func undim_loading():
	_load_dimmer.get_node("AnimationPlayer").play_backwards("dim_loading")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("undim_finished")
	_load_dimmer.get_node("Loading").visible = false
	
func dim_flash():
	_load_dimmer.get_node("AnimationPlayer").play("dim")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("dim_finished")
	_load_dimmer.get_node("AnimationPlayer").play_backwards("dim")
	yield(_load_dimmer.get_node("AnimationPlayer"), "animation_finished")
	emit_signal("undim_finished")
