#Description:
#This script is for storing global data and providing common utility functions

extends Node

const magnet_C = 256
const tile_unit_size = 64

var debug = false #managed by config.gd

var can_quit_from_app = true
var can_quit_from_os = true
var _requested_quit_delay = []

var player_ref = null

var audio_master = 0
var audio_ambient = 0
var audio_music = 0
var audio_effects = 0

signal QUIT # emmited when valid quit reques is given - gives chance for other nodes to request quit delay
var _quit_requester = false # false - none / null - OS / Object - app (node)
var _waiting = false

#func _init():
#	print(remap_value_between_ranges(100, Vector2(0,100),  Vector2(-80,6)))


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		quit(null)


func is_negligible(value):
	return abs(value) < 0.15


func gimme_tree():
	return get_tree()


func remap_value_between_ranges(value: float, old_range: Vector2, new_range: Vector2):
	return ((value - old_range.x) / (old_range.y - old_range.x)) * (new_range.y - new_range.x) + new_range.x


func get_all_hat_names():
	var dir = Directory.new()
	var regex = RegEx.new()
	dir.open("res://Scripts/Characters/Player/Hats/")
	if dir.list_dir_begin(true) == OK:
		var content = []
		while true:
			var file = dir.get_next()
			if file != "":
				regex.compile("^\\D+Hat\\.gdc?$")
				var result = regex.search(file)
				if result:
					regex.compile("\\.gdc?$")
					result = regex.sub(result.get_string(), "")
					content.append(result)
				else:
					regex.compile("^\\D+Hat\\.gd\\.remap$")
					result = regex.search(file)
					if not result:
						printerr("Found invalid hat name in 'res://Scripts/Characters/Player/Hats/' file: '" + file + "'")
			else:
				break
		dir.list_dir_end()
		return content
	push_error("Globals.gd can't get all hats for some reason ffs")
	return null


func is_hat_name_valid(hat):
	var file = File.new()
	hat = file.file_exists("res://Scripts/Characters/Player/Hats/" + hat + ".gd") or file.file_exists("res://Scripts/Characters/Player/Hats/" + hat + ".gdc")
	file.close()
	return hat


# param source - when calling this function enter self as parameter (null = OS)
func quit(source: Object):
	if typeof(_quit_requester) == 1: # bool = no quit request yet
		
		if (can_quit_from_app and source) or (can_quit_from_os and not source):
			_quit_requester = source
			if source:
				print("\nQuit request received from App: " + source.name + " (" + str(source) + ")")
			else:
				print("\nQuit request received from OS")
			emit_signal("QUIT")
			_wait_for_unresolved_quit_requests()
		
		elif source:
			printerr("Quit request received from App: " + source.name + " (" + str(source) + ") but was ignored (can_quit_from_app=false)")
		else:
			printerr("Quit request received from OS but was ignored (can_quit_from_os=false)")
	
	else: #already processing quit request
		pass
		if typeof(source) == 0:
			if typeof(_quit_requester) == 0:
				printerr("Quit request received from OS but was ignored (already processing quit request from OS)")
			elif typeof(_quit_requester) == 17:
				printerr("Quit request received from OS but was ignored (already processing quit request from App: " + _quit_requester.name + " (" + str(_quit_requester) + "))")
				
		elif typeof(source) == 17:
			if typeof(_quit_requester) == 0:
				printerr("Quit request received from App: " + source.name + " (" + str(source) + ") but was ignored (already processing quit request from OS)")
			elif typeof(_quit_requester) == 17:
				printerr("Quit request received from App: " + source.name + " (" + str(source) + ") but was ignored (already processing quit request from App: " + _quit_requester.name + " (" + str(_quit_requester) + "))")


# Force quit
# Use this only for debug purposes and then leave the quit requester do it at its own
# param source - when calling this function enter self as parameter
func _quit(source: Object):
	if source:
		print("\nForce quit request received from App: " + source.name + " (" + str(source) + ")")
	print("Quit")
	get_tree().quit()


# param source - when calling this function enter self as parameter
func quit_delay_request(source: Object):
	if not source in _requested_quit_delay:
		print("Quit delay request by: " + source.name + " (" + str(source) + ")")
		_requested_quit_delay.push_back(source)
	else:
		printerr("Quit requested already requested by: " + source.name + " (" + str(source) + ")")


# param source - when calling this function enter self as parameter
func cancel_quit_delay_request(source: Object):
	if source in _requested_quit_delay:
		print("Quit delay request canceled by : " + source.name + " (" + str(source) + ")")
		_requested_quit_delay.erase(source)
		if _requested_quit_delay.empty():
			_wait_for_unresolved_quit_requests()


func _wait_for_unresolved_quit_requests():
	if not _waiting:
		_waiting = true
		yield(get_tree().create_timer(0.1), "timeout") #wait for 100ms
		if _requested_quit_delay.empty():
			print("Quit delay requests resolved")
			_quit(null)
		else:
			_waiting = false
			print("Waiting for quit delay cancelation from: " + str(_requested_quit_delay))
