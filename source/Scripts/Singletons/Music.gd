extends Node

#Yeah... This is totally broken :(

# My sanity is gone. All I have left is misery
# I was once optimistic of this project, but times have changed.
# I just want to work on something with a real future.
# Nobody here believes in this project anymore. Not even the original creators.
# The goal is just to release this by some unreasonable deadline no matter how it looks.
# I am just ashamed by what has this project become.
# To anyone playing this game... I'm sorry
# Jakub Janšta


# NOTE: volume 80 is 100% and 100 is like 120%
# TransitionType graph: https://www.reddit.com/r/godot/comments/dgh9vd/transitiontype_cheat_sheet_tween_interpolation_oc/
#
# TransitionType: 
# TRANS_LINEAR = 0  --- The animation is interpolated linearly.
# TRANS_SINE = 1    --- The animation is interpolated using a sine function.
# TRANS_QUINT = 2   --- The animation is interpolated with a quintic (to the power of 5) function.
# TRANS_QUART = 3   --- The animation is interpolated with a quartic (to the power of 4) function.
# TRANS_QUAD = 4    --- The animation is interpolated with a quadratic (to the power of 2) function.
# TRANS_EXPO = 5    --- The animation is interpolated with an exponential (to the power of x) function.
# TRANS_ELASTIC = 6 --- The animation is interpolated with elasticity, wiggling around the edges.
# TRANS_CUBIC = 7   --- The animation is interpolated with a cubic (to the power of 3) function.
# TRANS_CIRC = 8    --- The animation is interpolated with a function using square roots.
# TRANS_BOUNCE = 9  --- The animation is interpolated by bouncing at the end.
# TRANS_BACK = 10   --- The animation is interpolated backing out at ends.
#
# EaseType:
# EASE_IN = 0       --- The interpolation starts slowly and speeds up towards the end.
# EASE_OUT = 1      --- The interpolation starts quickly and slows down towards the end.
# EASE_IN_OUT = 2   --- The interpolation is slowest at both ends. A combination of EASE_IN and EASE_OUT.
# EASE_OUT_IN = 3   --- The interpolation is fastest at both ends. A combination of EASE_IN and EASE_OUT.

var audiot_stream_player: AudioStreamPlayer
var tween: Tween
var current: AudioStream = null

var volume setget set_volume, get_volume #QOL just so you don't have to use the functions

signal fade_finished


func _init():
	audiot_stream_player = AudioStreamPlayer.new()
	tween = Tween.new()
	
	audiot_stream_player.bus = "Music"
	
	add_child(audiot_stream_player)
	add_child(tween)


func play(song_path: String):
	var file = File.new()
	if file.file_exists(song_path):
		play_stream(load(song_path)) #dirty fix and I don't care
#		print(ResourceQueue.callbacks)
#		ResourceQueue.register_callback(song_path, "is_ready", funcref(self, "_play_stream_from_ResourceQueue_helper"))
#		ResourceQueue.queue_resource(song_path)
#		printerr("progress> ", ResourceQueue.get_progress(song_path))
#		yield(get_tree().create_timer(5), "timeout")
#		printerr("progress> ", ResourceQueue.get_progress(song_path))
#		print(ResourceQueue.callbacks)
	else:
		push_error("Song '" + song_path + "' not found.")
	file.close()


func play_stream(audio_stream: AudioStream):
	current = audio_stream
#	audio_stream.loop = false #Either crashes or does nothing at all ¯\_(ツ)_/¯
	audiot_stream_player.stream = audio_stream
	audiot_stream_player.playing = true


func _play_stream_from_ResourceQueue_helper(resource_path: String):
	play_stream(ResourceQueue.get_resource(resource_path))

func set_volume(Volume: float):
	audiot_stream_player.volume_db = clamp(Globals.remap_value_between_ranges(Volume, Vector2(0,100), Vector2(-80, 20)), -80, 20)

func get_volume():
	return Globals.remap_value_between_ranges(audiot_stream_player.volume_db, Vector2(-80, 20), Vector2(0, 100))

func stop():
	current = null
	audiot_stream_player.playing = false

func pause():
	audiot_stream_player.playing = false

func resume():
	if not current:
		printerr("No current song to continue. Use Music.play() or Music.play_stream() to set a current song")
		return
	audiot_stream_player.playing = true

#This does not work at all

# warning-ignore:return_value_discarded
# warning-ignore:return_value_discarded
# warning-ignore:return_value_discarded
func fade(from: float=0, to: float=100, duration: float=1, transition_type: int=Tween.TRANS_LINEAR, ease_type: int=Tween.EASE_IN):
#	audiot_stream_player.volume_db = clamp(Globals.remap_value_between_ranges(from, Vector2(0,100), Vector2(-80, 20)), -80, 20)
	# Long boiii
	tween.interpolate_property(audiot_stream_player, "volume_db", clamp(Globals.remap_value_between_ranges(from, Vector2(0,100), Vector2(-80, 20)), -80, 20), clamp(Globals.remap_value_between_ranges(to, Vector2(0,100), Vector2(-80, 20)), -80, 20), duration, transition_type, ease_type)
	tween.interpolate_callback(self, duration, "_fade_finished")
	tween.start()

func fade_to(to: float, duration: float=1, transition_type: int=Tween.TRANS_LINEAR, ease_type: int=Tween.EASE_IN):
	fade(audiot_stream_player.volume_db, to, duration, transition_type, ease_type)

func fade_out(duration: float=1, transition_type: int=Tween.TRANS_LINEAR, ease_type: int=Tween.EASE_IN):
	fade_to(0, duration, transition_type, ease_type)

# warning-ignore:return_value_discarded
func _fade_finished():
	tween.stop_all()
	emit_signal("fade_finished")
	if audiot_stream_player.volume_db < 0:
		audiot_stream_player.volume_db = -80
