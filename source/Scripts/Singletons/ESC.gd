extends Node

var esc_overlay = preload("res://Scenes/UI/Canvas/Esc_screen.tscn").instance()
# modes:
# 0: Forbidden
# 1: Can display Esc overlay
var mode = 1 setget set_mode
var visible = false setget set_visible


func set_visible(_mode):
	return

func _ready():
	Window.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	set_pause_mode(2)
	add_child(esc_overlay)

func _input(event):
	if event is InputEventKey and event.pressed and not event.is_echo() and event.scancode == KEY_ESCAPE:
		if mode:
			if visible:
				hide()
			else:
				show()

func set_mode(new=1):
	mode = 0 if new == 0 else 1


func show():
	if mode:
		visible = true
		get_tree().paused = true
		Window.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		esc_overlay.get_node("EscMenu").mouse_filter = Control.MOUSE_FILTER_STOP
		esc_overlay.get_node("AnimationPlayer").play("menu_up")

func hide():
	visible = false
	get_tree().paused = false
	Window.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	esc_overlay.get_node("EscMenu").mouse_filter = Control.MOUSE_FILTER_PASS
	esc_overlay.get_node("AnimationPlayer").play("menu_down")
