extends Control

var multipliers = [1,5,10,15,20,35,17]

var viewport_size = Vector2(1280,720)
var relative_x = 0
var relative_y = 0

var _next_animation = null

var rick = false

func _ready():
#	Music.play("res://Resources/Audio/Music/main_menu.wav")
	PersistentMusic.player.stream = preload("res://Resources/Audio/Audio/songs/Main theme/Main Menu.wav")
	PersistentMusic.player.pitch_scale = 0.8
	PersistentMusic.player.play()
	Esc.mode = 0
	Window.set_window_always_on_top(false)
	Window.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	Window.set_window_size(Vector2(1280,720))
	OS.center_window()
	
	get_viewport().warp_mouse(get_viewport_rect().size/2)
	
	yield(get_tree().create_timer(.3), "timeout")
	$AnimationPlayer.play("main_down")
	



func _process(delta):
	var layer = 0
	for child in $ParallaxBackground.get_children(): # for each parallaxlayer do...
		child.motion_offset.x = lerp(child.motion_offset.x, multipliers[layer] * relative_x, 5 * delta)
		child.motion_offset.y = lerp(child.motion_offset.y, multipliers[layer] * relative_y, 5 * delta)
		layer += 1

func _input(event):
	if event is InputEventMouseMotion:
		relative_x = -1 * (event.position.x - (viewport_size.x/2)) / (viewport_size.x/2)
		relative_y = -1 * (event.position.y - (viewport_size.y/2)) / (viewport_size.y/2)
	if event is InputEventKey and event.pressed and not event.is_echo() and event.scancode == KEY_P and not rick:
		$CanvasLayer/AcceptDialog.popup()
#		print("RICKROLLED! EKS DEE DEE DEE DEE DEE DEE DEE...")
#		# warning-ignore:return_value_discarded
#		OS.shell_open("https://www.youtube.com/watch?v=dQw4w9WgXcQ")


#func _on_Play_released():
#	$AnimationPlayer.play("main_up")
#	_next_animation = "play_down"

func _on_Play_released():
	$AnimationPlayer.play("main_up")
	SceneChanger.change_scene("res://Scenes/UI/Screens/intro.tscn")

func _on_Credits_released():
	$AnimationPlayer.play("main_up")
	SceneChanger.change_scene("res://Scenes/UI/Screens/Credits.tscn")
	PersistentMusic.player.stop()

func _on_Exit_released():
#	$AnimationPlayer.play("main_up")
	Globals.quit(self)


#func _on_Continue_released():
#	$AnimationPlayer.play("main_up")
##	Music.fade_out(2)
#	SceneChanger.change_scene("res://Scenes/Levels/CloseT/CloseT.tscn")

#func _on_NewGame_released():
#	$AnimationPlayer.play("main_up")
##	Music.fade_out(2)
#	SceneChanger.change_scene("res://Scenes/UI/Screens/intro.tscn")

#func _on_Back_released():
#	$AnimationPlayer.play("main_up")
#	_next_animation = "main_down"


func _on_AnimationPlayer_animation_finished(_anim_name):
	if _next_animation:
		$AnimationPlayer.play(_next_animation)
		_next_animation = null
