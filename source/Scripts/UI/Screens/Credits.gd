extends ColorRect

var timer

func _ready():
	PersistentMusic.player.stop()
	timer = Timer.new()
	timer.connect("timeout",self,"hide")
	timer.wait_time = 3
	timer.one_shot = true
	add_child(timer)
	Esc.mode = 0
	Window.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$AnimationPlayer.play("credits")

func _input(event):
	if event is InputEventKey and event.pressed and not event.is_echo():
		if $Skip.visible:
			if event.scancode == KEY_ESCAPE:
				SceneChanger.change_scene("res://Scenes/UI/Menus/Main_menu.tscn")
		else:
			$Skip.visible = true
			timer.start()

func hide():
	$Skip.visible = false


func _on_AudioStreamPlayer_finished():
	SceneChanger.change_scene("res://Scenes/UI/Menus/Main_menu.tscn")
