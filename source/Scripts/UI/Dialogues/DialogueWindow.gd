extends CanvasLayer

const option_preload = preload("res://Scenes/UI/Dialogues/Option.tscn")
const option_spacing = 115
var options = []
var selected = 0

var _dialogue = null
var active = true

#connect these from NPC
signal trigger(name)
signal var_changed(variable_name, new_value, previous_value)
signal tags(tags)
signal dialogue_ended

#usage:
#      1. instance this scene and add it to the tree
#      2. use load_dialogue() (step for the first time to activate)
#      3. use open() / close()
#      4. use step() 
#      5. use connected dialogue_ended signal to close the dialogue or do whatever you want with with it

func _init():
	add_to_group("Clyde")
	_dialogue = ClydeDialogue.new()
	add_child(_dialogue)
	_dialogue.connect("event_triggered", self, '_on_event_triggered')
	_dialogue.connect("variable_changed", self, '_on_variable_changed')

func _ready():
	close()
	$Options.hide()

func load_dialogue(dialogue):
	_dialogue.load_dialogue(dialogue)
	start()

func start():
	_dialogue.start()
	active = true
	step()
	active = false

# {id:Null, speaker:player, tags:Null, text:hi, type:line}
func step():
	if active:
		var content = _dialogue.get_content()
#		print(content)
		if content:
			_update_speaker_portrait(content.speaker)
			if content.tags:
				emit_signal("tags", content.tags as Array)
			
			if content.type == "line":
				_show_line(content)
			elif content.type == "options":
				_show_options(content)
		else:
			emit_signal("dialogue_ended")
#			close() #manage this individually

func open():
	$Window.visible = true
	active = true
	Window.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
	Esc.mode = 0
	GameState.setv("player.canMove", false)
	GameState.setv("hat.isEnabled", false)


func close():
	$Window.visible = false
	active = false
	Window.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	Esc.mode = 1
	yield(get_tree().create_timer(.2), "timeout") #just to make sure player doesn't jump at the end of conversations
	GameState.setv("player.canMove", true)
	GameState.setv("hat.isEnabled", true)


func _show_line(content):
	$Options.hide()
	$Window/SpeakerLabel.text = content.speaker
	$Window/Label.text = content.text


func _show_options(content):
	$Options.show()
	$Window/SpeakerLabel.text = content.speaker
	$Window/Label.text = content.name
	
	for option in $Options.get_children():
		option.queue_free()
	
	for i in range(content["options"].size()):
		var option_instance = option_preload.instance()
		option_instance.set_up(i, content["options"][i]["label"])
		option_instance.connect("pressed", self, "_choose")
		option_instance.connect("selected", self, "_select")
		
		$Options.add_child(option_instance)
		option_instance.rect_position.y -= (content["options"].size()-i) * option_spacing
		options.push_back(option_instance)
	
	options[0].select(true)


func _choose(index): #choose option and confirm it
	if active:
		options = []
		_dialogue.choose(index)
		step()


func _select(id): #just hover over option and display cursor
	if active:
		selected = id
		for option in options:
			option.select(false)
		options[id].select(true)


func _update_speaker_portrait(speaker):
	if speaker and speaker in $Window/Portrait.frames.get_animation_names():
		$Window/Portrait.play(speaker)
	else:
		printerr("Portrait animation for speaker '"+speaker+"' does not exits. Using default" if speaker else "Null speaker. Using default")
		$Window/Portrait.play("Default")


func _input(event):
	#press any key to continue
	if (event is InputEventKey or event is InputEventMouseButton) and event.pressed and not event.is_echo():
		if $Options.visible:
			if event.is_action_pressed("dialogue_up"):
				_select(clamp(selected-1, 0, options.size()-1))
			elif event.is_action_pressed("dialogue_down"):
				_select(clamp(selected+1, 0, options.size()-1))
			elif event.is_action_pressed("dialogue_select"):
				_choose(selected)
		elif event.is_action_pressed("dialogue_continue") or event is InputEventMouseButton:
			step()


func _on_event_triggered(event_name):
	if event_name == "exit":
		close()
	else:
		emit_signal("trigger", event_name)

func _on_variable_changed(variable_name, new_value, previous_value):
	emit_signal("var_changed", variable_name, new_value, previous_value)
