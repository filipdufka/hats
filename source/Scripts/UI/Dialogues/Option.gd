extends Control
var id = 0
signal selected(id)
signal pressed(id)

func _ready():
	Window.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func set_up(option_id=0, option_text="Sample option text"):
	id = option_id
	$Label.text = option_text

func select(pointer: bool):
	$Sprite.scale = Vector2(.33,.23) if pointer else Vector2(.3,.2)

func _on_Option_mouse_entered():
	emit_signal("selected", id)

func _on_Option_gui_input(event):
	if event is InputEventMouseButton and not event.pressed:
		emit_signal("pressed", id)
