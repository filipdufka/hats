extends Node2D

signal pressed
signal released
signal focus
signal unfocus 

export(Texture) var texture setget set_texture
export(Vector2) var default_scale = Vector2(1,1) setget set_scale
export(Vector2) var hover_scale = Vector2(1.1,1.1)
export(bool) var active = true setget set_active

func _ready():
	set_active(active)

func set_texture(tex):
	texture = tex
	$Sprite.texture = tex

func set_scale(sc):
	scale = sc
	default_scale = sc

func _on_MouseCapture_gui_input(event):
	if event is InputEventMouseButton and active:
		if event.is_pressed() and not event.is_echo():
			scale = default_scale
			emit_signal("pressed")
		elif not event.is_pressed():
			scale = hover_scale
			yield(get_tree().create_timer(.08), "timeout")
			emit_signal("released")

func _on_MouseCapture_mouse_entered():
	if active:
		scale = hover_scale
		emit_signal("focus")

func _on_MouseCapture_mouse_exited():
	if active:
		scale = default_scale
		emit_signal("unfocus")

func set_active(mode: bool):
	if mode:
		modulate = Color(1,1,1,1)
	else:
		modulate = Color(1,1,1,.8)
		scale = default_scale
	active = mode
