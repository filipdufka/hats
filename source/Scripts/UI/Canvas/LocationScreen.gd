extends CanvasLayer


#Better way than exporting paths

export (String) onready var location_


onready var HeavenIcon = preload("res://Resources/Visuals/Locations/Heaven_ICON.png")
onready var NDIcon = preload("res://Resources/Visuals/Locations/ND_ICON.png")
onready var SlumsIcon = preload("res://Resources/Visuals/Locations/Slums_ICON.png")

var CurrentIco = null
var locationCheck = false

func _ready():
	$Control/ColorRect.visible = false
	$LocationIcon.visible = false
	
	passLocation(location_)




func passLocation(loc) -> void:
	
#Match Icons and Location checks
	match loc:
		"Heaven":
			CurrentIco = HeavenIcon
			locationCheck = LocationVisitedCheck.Heaven_visited
		"ND":
			CurrentIco = NDIcon
			locationCheck = LocationVisitedCheck.ND_visited
		"Slums":
			CurrentIco = SlumsIcon
			locationCheck = LocationVisitedCheck.Slums_visited


#	If location Unvisited - show

	if locationCheck == false:
		match loc:
			"Heaven":
				LocationVisitedCheck.Heaven_visited = true
			"ND":
				LocationVisitedCheck.ND_visited = true
			"Slums":
				LocationVisitedCheck.Slums_visited = true

		$LocationIcon.texture = CurrentIco
		show()


func show():
	$AnimationPlayer.play("Show")
