extends CanvasLayer

onready var label = $FPS
const C180 = Color("00FF7F")
const C120 = Color("3BCA6D")
const C60 = Color("77945C")
const C30 = Color("B25F4A")
const Cpotato = Color("ED2938")


func _process(_delta):
	var fps = Engine.get_frames_per_second()
	if fps > 120:
		label.modulate = C180
	elif fps > 90:
		label.modulate = C120
	elif fps > 45:
		label.modulate = C60
	elif fps > 15:
		label.modulate = C30
	else:
		label.modulate = Cpotato
	label.text = "fps: " + str(fps)
