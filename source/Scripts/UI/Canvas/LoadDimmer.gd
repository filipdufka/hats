extends CanvasLayer

#loading screen tips
const lst = [
	"Did you know, that you can press 'P' in the main menu?",
	"Did you know, that you can press 'P' in the main menu?",
	"57 6f 77 2e 2e 2e 20 59 6f 75 20 63 61 6e 20 75 73 65 20 61 6e 20 6f 6e 6c 69 6e 65 20 63 6f 6e 76 65 72 74 6f 72",
	"Made with Godot",
	'" Beep beep boop beep "\n                                -Lapse',
	"A game about hats I guess...",
	"It's hats o'clock",
	"Is cereal a soup?",
	"A blind robot walked into a bar… and a table… and a chair…",
	"I made all the assets against my will",
	"No artist were hurt during the development",
	"Never pee in a dream... It's a trap!",
	"Did you know, that you can press 'P' in the main menu?",
	"Did you know, that you can press 'P' in the main menu?",
	"+[shake rate=10 level=10][rainbow freq=2 sat=15 val=1]Rainbow![/rainbow][/shake]",
	"Despite being a comedy, Hats does not feature any jokes",
	"Do not try to re-create any of these hats at home",
	"Attempted theft is still theft. But don't worry about that",
	"Close-T's Euqene-9 is well known to be a ladies man",
	"Have you SEEN the giant ball in the sky?",
	"Cloudon sits on the head of a giant pillar. You know, like a hat.",
	"If you get lost, try being smarter",
	"If you get stuck, try being better at the game",
	"If you cannot find any hats, try finding some more hats",
	"Most of the in-game dialogues contain a fair amount of unspoken swearing",
	"tip: Please don't wear fedoras",
	'" When we are tired we are attacked by the ideas we have conquered long ago. "\n                                                                                         -Friedrich Nietzsche',
	'" Gears are diffrent but all has a same purpose. "\n                                                                   -Marhtha Luther Gear',
	"More espresso less depresso",
	"Hat is useless without head",
	"I demand my own caffeine hat!",
	'You cannot spell "organophosphate" without "hat". Damn.',
	"Did you know, that you can press 'P' in the main menu?",
	"Did you know, that you can press 'P' in the main menu?",
	]

func _init():
	randomize()

func randon_lst():
	var tip = lst[randi() % lst.size()]
	if tip[0] == "+": #is bbcode
		$Loading/Label.self_modulate = Color("00ffffff")
		$Loading/Label/RichTextLabel.visible = true
		$Loading/Label/RichTextLabel.bbcode_text = tip.substr(1)
	else:
		$Loading/Label.text = tip
