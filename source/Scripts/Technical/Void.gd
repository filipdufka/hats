extends Area2D


#Only intented for Level_Nd_exhibiton
#Do not turn on anywhere else
export (bool) onready var DoorCrush

func _on_Void_body_entered(body):
	if body == Globals.player_ref:
		# warning-ignore:return_value_discarded
		match DoorCrush:

#Death by being crushed
			true:
				GameOverScreen._fade_in(2)
				body.die()
				
#Death by Falling
			false:
				GameOverScreen._fade_in(3)
				body.die()
				
