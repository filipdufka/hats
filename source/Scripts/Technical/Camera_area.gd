extends Area2D

export(Vector2) var zoom = Vector2(1.8,1.8)
export(int) var left = -10000000
export(int) var top = -10000000
export(int) var right = 10000000
export(int) var bottom = 10000000

func _on_Area2D_body_entered(body):
	if body == Globals.player_ref:
		body.get_node("Camera").limit_left = left
		body.get_node("Camera").limit_top = top
		body.get_node("Camera").limit_right = right
		body.get_node("Camera").limit_bottom = bottom
		body.get_node("Camera").zoom = zoom
