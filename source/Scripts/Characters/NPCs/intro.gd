extends NPC

var in_range = false

func _ready():
	_set_up($AnimatedSprite, $AudioStreamPlayer, "1. opening_narratoion.clyde")
	dialogue.connect("dialogue_ended", self, "_handle_dialogue_end")

func _on_AreaTrigger_trigger_entered():
	dialogue.open()

func _handle_dialogue_end():
	dialogue.start() #restart - why tho?
	dialogue.close() #close after finishing
#	get_tree().change_scene("res://Scenes/Levels/Slums/Slums.tscn")
	SceneChanger.change_scene("res://Scenes/Levels/Slums/Slums.tscn")
