extends NPC 

var in_range = false

func _ready():
	_set_up($AnimatedSprite, $AudioStreamPlayer2D, "zlatyklobouik.clyde")
	dialogue.connect("dialogue_ended", self, "_handle_dialogue_end")

func _on_AreaTrigger_trigger_entered():
	$PromptBody/Prompt.text = "PRESS '"+ InputMap.get_action_list("interact")[0].as_text() + "'"
	$PromptBody.show()
	in_range = true

func _on_AreaTrigger_trigger_exited():
	$PromptBody.hide()
	in_range = false

func _input(event):
	if not in_range: 
		return
	if event.is_action_pressed("interact"):
		dialogue.open()

func _handle_dialogue_end():
	dialogue.start() #restart
	dialogue.close() #close after finishing
	GameState.unlock_hat("TimeHat")
	Globals.player_ref.set_hat("TimeHat")
#	get_tree().change_scene("res://Scenes/Levels/Heaven/Level_Heaven_5_2.tscn")
	SceneChanger.change_scene("res://Scenes/Levels/Heaven/Level_Heaven_5_2.tscn")
