extends Node2D
class_name NPC

#set in _ready() of instanced NPC
var gfx: AnimatedSprite
var aud: AudioStreamPlayer2D
var dialogue = preload("res://Scenes/UI/Dialogues/Dialogue_window.tscn").instance() setget set_dialogue, get_dialogue


#Override for specific NPC
var sound_pool = {
	"sample": preload("res://Resources/Audio/Dialogues/sound.wav")
}


func _init():
	add_child(dialogue)
	dialogue.connect("trigger", self, "trigger")
	dialogue.connect("tags", self, "tags")
	dialogue.connect("tags", self, "_do_tags")
	dialogue.connect("var_changed", self, "var_changed")


#Call this in _ready() of every NPC instance before anything else
func _set_up(animatedSprite: AnimatedSprite, audioStreamPlayer2D: AudioStreamPlayer2D, Dialogue: String):
	gfx = animatedSprite
	aud = audioStreamPlayer2D
	set_dialogue(Dialogue)

#Override for specific NPC

# warning-ignore:unused_argument
func trigger(name):
	pass
# warning-ignore:unused_argument
func tags(tags):
	pass
# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func var_changed(var_name, new_value, prev_value):
	pass


func play_animation(animation: String):
	if animation in gfx.frames.get_animation_names():
		if animation != gfx.animation:
			gfx.animation = animation
	else:
		push_error("No animation '"+animation+"' for this NPC found")


func play_sound(sound: String):
	if sound in sound_pool:
		aud.stop()
		aud.set_stream(sound_pool[sound])
		aud.play(0)
	else:
		push_error("No sound '"+sound+"' for this NPC found")
	
func _do_tags(tags: Array): #Sould be array of only strings (not PoolStringArray)                                                                                                                      Juan pls gimme typed arrays already
	for tag in tags:
		tag = tag.split("_", false)
		if tag.size() == 2:
			match tag[0]:
				"sound":
					play_sound(tag[1])
				"animation": 
					play_animation(tag[1])
				#TODO some special actions if necessary

func set_dialogue(Dialogue: String):
	dialogue.load_dialogue(Dialogue)
#	if Directory.new().file_exists("res://Dialogues/"+Dialogue):
#		dialogue.load_dialogue(Dialogue)
#	else:
#		push_error("Dialogue not found 'res://Dialogues/"+Dialogue+"'")

func get_dialogue():
	return dialogue._dialogue.active_dialoque
