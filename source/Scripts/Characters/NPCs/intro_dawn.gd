extends NPC

var in_range = false

func _ready():
	_set_up($AnimatedSprite, $AudioStreamPlayer2D, "4. Transition_Dawntown.clyde")
	dialogue.connect("dialogue_ended", self, "_handle_dialogue_end")

func _on_AreaTrigger_trigger_entered():
		dialogue.open()

func _handle_dialogue_end():
	dialogue.start() #restart
	dialogue.close() #close after finishing


func _on_AreaTrigger_trigger_exited():
	pass # Replace with function body.
