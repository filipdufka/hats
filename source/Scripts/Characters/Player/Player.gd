extends KinematicBody2D
const controller_type = 0

#TODO: coyote timer, slope stopper (raycast collision normal angle?), better ground detection on round edges (testmove to the sides), camera, snap na rampy

#public and editor settings
export (bool) var jumping_enabled = true
export (bool) var start_facing_right = false
export (float, 0.01, 100) var speed_tiles_per_sec = 3.6
export (float, 0, 1) var horizontal_lerp_weight = 0.6
export (float, 0.01, 100) var max_jump_height = 2.5
export (float, 0.01, 100) var min_jump_height = 0.5
export (float, 0.01, 60) var sec_jump_duration = 0.5
export (float, 0, 60) var sec_jump_buffer = 0.2
export (float, 0, 60) var sec_coyote_time = 0.1
export (float, 0, 60) var sec_airTime_time = 0.175
#export (float) var ext_forces_damp = 0.1

#changing value of Collision Safe Margin may be required if physics collisions appears buggy
export (float) var physics_push_bounce_force = 75

#private variables basen on public settings
var _gravity
var _max_jump_velocity
var _min_jump_velocity
var _lerp_weight
var _speed
var _push_bounce_force

#script global woking variables
var _velocity = Vector2(0,0)
#var _external_forces = Vector2(0,0)
var is_grounded = false

enum{IDLE, WALK, RUN, AIR, JUMP, LAND, DANCE}
var state = IDLE
var floor_anle = 0
var direction = 0

var can_dance = false

#subnode cache
onready var GFX = $GFX
#onready var coyoteTimer = Timer.new()
onready var jumpBufferTimer = Timer.new()
onready var airTimer = Timer.new()


func _ready():
	GFX.flip_h = start_facing_right
#	add_child(coyoteTimer)
	add_child(jumpBufferTimer)
	add_child(airTimer)
	airTimer.connect("timeout", self, "airTimer_timeout")
	
	recalculate_movement_settings()
	self.add_to_group("Characters")
	GFX.set_lapse_animation("idle")
#	set_hat(GameState.getv("hat.active"))


func _enter_tree():
	Globals.player_ref = self

func _exit_tree():
	Globals.player_ref = null


func recalculate_movement_settings():
	_gravity = 2 * Globals.tile_unit_size * max_jump_height / pow(sec_jump_duration, 2)
	_max_jump_velocity = -sqrt(2 * _gravity * max_jump_height * Globals.tile_unit_size)
	_min_jump_velocity = -sqrt(2 * _gravity * min_jump_height * Globals.tile_unit_size)
	_lerp_weight = 1 - horizontal_lerp_weight
	_speed =  speed_tiles_per_sec * Globals.tile_unit_size
	_push_bounce_force = physics_push_bounce_force if physics_push_bounce_force > 0 else 0
#	coyoteTimer.wait_time = sec_coyote_time
	jumpBufferTimer.wait_time = sec_jump_buffer
	airTimer.wait_time = sec_airTime_time
#	coyoteTimer.one_shot = true
	jumpBufferTimer.one_shot = true
	airTimer.one_shot = true


func _physics_process(delta):
	direction = int(GameState.getv("player.canMove")) * (-Input.get_action_strength("move_left") + Input.get_action_strength("move_right"))
	_velocity.x = lerp(_velocity.x, _speed * direction, _lerp_weight)
	#flip sprite
	if direction == 1:
		GFX.flip_h = true
	elif direction == -1:
		GFX.flip_h = false
	
	
	#activate jumpBuffer
	if Input.is_action_just_pressed("move_jump") and jumping_enabled:
		jumpBufferTimer.start()
	#jump if jumpBuffer is active
	if is_grounded:
		if not jumpBufferTimer.is_stopped() and GameState.getv("player.canMove"):
			jumpBufferTimer.stop()
			_velocity.y = _max_jump_velocity
			state = JUMP
	else:
		#gravity
		_velocity.y += _gravity * delta
	
	#variable jump cutoff
	if (not Input.is_action_pressed("move_jump") and _velocity.y < _min_jump_velocity) and jumping_enabled:
		_velocity.y = _min_jump_velocity
	
	
	#Garbaj
	match state:
		IDLE:
			if $Timer.is_stopped():
				$Timer.start()
			GFX.set_lapse_animation("idle")
			if direction:
				state = WALK
			if not is_grounded and airTimer.is_stopped():
				airTimer.start()
			
		WALK:
			$Timer.stop()
			GFX.set_lapse_animation("walk")
			if not direction:
				state = IDLE
			if not is_grounded:
				airTimer.start()
			
		RUN:
			$Timer.stop()
			GFX.set_lapse_animation("run")
			if not direction:
				state = IDLE
			if not is_grounded:
				airTimer.start()
		
		JUMP:
			$Timer.stop()
			GFX.set_lapse_animation("jump")
			
		AIR:
			$Timer.stop()
			GFX.set_lapse_animation("air")
			if is_grounded:
				state = LAND
			
		LAND:
			$Timer.stop()
			GFX.set_lapse_animation("land")
			
		DANCE:
			GFX.set_lapse_animation("dance")
			if direction != 0 or not GameState.getv("player.canMove"):
				state = IDLE
	
	#external forces
#	var desired_ext_forces = lerp(_external_forces, Vector2(0,0), ext_forces_damp)
#	_external_forces = desired_ext_forces if abs(desired_ext_forces.length()) > 0.1 else Vector2(0,0)
#	_velocity += _external_forces
	
	#moving
	var collision = move_and_collide(_velocity * delta, false)
	if collision:
		_velocity = _velocity.slide(collision.normal)
		if collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)
		var remainder = collision.remainder.slide(collision.normal)
		collision = move_and_collide(remainder)
		#These two lines are not critical, but make physics collisions a bit smoother
		if collision and collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)

	#Test move 1 unit down to determine if the player is grounded
	is_grounded = true if move_and_collide(Vector2.DOWN, false, true, true) else false


#func set_ext_forces(force: Vector2):
#	_external_forces = force
#
#func add_ext_force(force: Vector2):
#	_external_forces += force

func set_hat(new_hat="NullHat", force=false):
	if new_hat == null:
		new_hat = "NullHat"
	GFX.set_hat(new_hat, force)


func _on_GFX_animation_finished():
	#dirty fix for animation stutter
	if state == JUMP:
		GFX.set_lapse_animation("air")
		state = AIR
	elif state == LAND:
		GFX.set_lapse_animation("idle")
		state = IDLE


func airTimer_timeout():
	if not is_grounded:
		state = AIR


#Hotfix - Need to add state -> Disabling Shape is not working, setting different coll layer and mask
#Absolute Garbage - called from Death cause object, have to call globally???
func die():
	self.visible = false
	set_physics_process(false)


func _on_Timer_timeout():
	if can_dance and GameState.getv("player.canMove"):
		print("DANCE FOR ME!")
		state = DANCE
