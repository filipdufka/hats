extends AnimatedSprite

var hat

#animation cache
const lapse_animations = { #CollisionShape index[0]=animation index[1]=position index[2]=radius, height 
	"idle"  : [preload("res://Resources/Visuals/Characters/Lapse/lapse_idle_hat.png"), Vector2(0,0), Vector2(0,0)],
	"walk"  : [preload("res://Resources/Visuals/Characters/Lapse/lapse_walk_hat.png"), Vector2(0,0), Vector2(0,0)],
#	"run"   : [preload("res://Resources/Visuals/Characters/Lapse/lapse_run_hat.png"), Vector2(0,0), Vector2(0,0)],
	"jump"  : [preload("res://Resources/Visuals/Characters/Lapse/lapse_jump_hat.png"), Vector2(0,0), Vector2(0,0)],
	"air"   : [preload("res://Resources/Visuals/Characters/Lapse/lapse_air_hat.png"), Vector2(0,0), Vector2(0,0)],
	"land"  : [preload("res://Resources/Visuals/Characters/Lapse/lapse_land_hat.png"), Vector2(0,0), Vector2(0,0)],
	"dance" : [preload("res://Resources/Visuals/Characters/Lapse/lapse_dance_hat.png"), Vector2(0,0), Vector2(0,0)],
}

#hat stuff cache
const hats = { #preloaded hats
	"BioHat"    : preload("res://Scripts/Characters/Player/Hats/BioHat.gd"),
	"DebugHat"  : preload("res://Scripts/Characters/Player/Hats/DebugHat.gd"),
	"EloqHat"   : preload("res://Scripts/Characters/Player/Hats/EloqHat.gd"),
	"HeliHat"   : preload("res://Scripts/Characters/Player/Hats/HeliHat.gd"),
	"MagnetHat" : preload("res://Scripts/Characters/Player/Hats/MagnetHat.gd"),
	"NullHat"   : preload("res://Scripts/Characters/Player/Hats/NullHat.gd"),
	"TeslaHat"  : preload("res://Scripts/Characters/Player/Hats/TeslaHat.gd"),
	"TimeHat"   : preload("res://Scripts/Characters/Player/Hats/TimeHat.gd"),
	"BasicHat"  : preload("res://Scripts/Characters/Player/Hats/BasicHat.gd"),
	"FakeGoldenHat": preload("res://Scripts/Characters/Player/Hats/FakeGoldenHat.gd"),
	"GoldenHat": preload("res://Scripts/Characters/Player/Hats/GoldenHat.gd")
}

func _init():
	if not hat:
		set_hat(GameState.getv("hat.active"))
	self.material.set_shader_param("hat_animation_pos", lapse_animations["idle"][0])


func _on_frame_changed():
	hat.next_frame()

#Hat part
#@param force: force equip a hat if in debug mode
func set_hat(new_hat="NullHat", force=false):
	if Globals.is_hat_name_valid(new_hat):
		if new_hat in GameState.getv("acquiredHats") or (force and Globals.debug):
			if hat:
				hat.clean_up()
			hat = hats[new_hat].new(self)
			GameState.setv("hat.active", new_hat)
			print("Hat '" + new_hat + ("' set FORCED" if force else "' set"))
		else:
			printerr("Hat '" + new_hat + "' is not yet acquired.")
			if not hat: #fail-safe
				GameState.unlock_hat("NullHat")
				set_hat("NullHat")
	else:
		printerr("Hat '" + new_hat + "' does not exist")
		if not hat: #fail-safe
			GameState.unlock_hat("NullHat")
			set_hat("NullHat")


func _physics_process(delta):
	hat._physics_process(delta)


func set_hat_animation(animation_name, once=false):
	hat.play_animation(animation_name, once)


#Lapse part
func set_lapse_animation(animation_name):
	if self.animation != animation_name:
		self.animation = animation_name
		self.material.set_shader_param("hat_animation_pos", lapse_animations[animation_name][0])

