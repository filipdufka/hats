extends Reference

#This hat is not meant to be worn

var name = "GenericHat"
var default_animation = "idle"

var animation
var last_animation = default_animation
var animation_frames = 0
var frame = 0

var _parent = null
var _once = false

var hat_animation_pool = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/generic_idle.png"), Vector2(1,1)]
} setget set_hat_animation_pool, get_hat_animation_pool
#because gdscript hates me
func set_hat_animation_pool(pool):
	hat_animation_pool = pool
func get_hat_animation_pool():
	return hat_animation_pool


func _init(hat_name: String, parent_reference: Object, animation_pool: Dictionary, starting_animation: String="idle"):
	name = hat_name 
	_parent = parent_reference
	hat_animation_pool = animation_pool
	default_animation = starting_animation
	play_animation(default_animation)


#There functions can be overriden
func set_up():
	pass
func clean_up():
	pass
func _physics_process(_delta):
	pass


func next_frame():
	_parent.material.set_shader_param("hat_frame", frame)
	frame = (frame+1) % int(animation_frames) # wrap back to 0
	if not frame: # animation started playing for second time
		if _once:
			_once = false
			play_animation(last_animation)

# if once == true the previous animation will be played after the animation finishes -> it is played once
func play_animation(animation_name, once=false):
	if animation != animation_name and animation_name in hat_animation_pool:
		_once = once
		last_animation = animation
		animation = animation_name
		animation_frames = (hat_animation_pool[animation][1].x * hat_animation_pool[animation][1].y)+1

		_parent.material.set_shader_param("hat_animation", hat_animation_pool[animation][0])
		_parent.material.set_shader_param("hat_animation_x", hat_animation_pool[animation][1].x)
		_parent.material.set_shader_param("hat_animation_y", hat_animation_pool[animation][1].y)
