extends "GenericHat.gd"

const hat_animations = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/generic_idle.png"), Vector2(1,1)]
}

func _init(parent_reference).("DebugHat", parent_reference, hat_animations):
	pass
