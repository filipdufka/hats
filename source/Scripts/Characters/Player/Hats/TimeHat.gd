extends "GenericHat.gd"

var time_stopped = false

# index 0: preloaded path
# index 1: rows anc collums in Vector2
const hat_animations = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/time_idle.png"), Vector2(2,2)]
}

func _init(parent_reference).("TimeHat", parent_reference, hat_animations):
	pass

func clean_up():
	for object in Globals.gimme_tree().get_nodes_in_group("timeStoppable"):
		object.time_stopped(false)

func _physics_process(_delta):
	if Input.is_action_just_pressed("hat_action_1"):
		if GameState.getv("hat.isEnabled"):
			time_stopped = !time_stopped
			for object in Globals.gimme_tree().get_nodes_in_group("timeStoppable"):
				object.time_stopped(time_stopped)
