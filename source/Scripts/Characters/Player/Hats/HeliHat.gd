extends "GenericHat.gd"


var _active = false setget set_active
#Stored from previous state
var _stored_max_jump_height
var _stored_min_jump_height
var _stored_sec_jump_duration
#Configuration of HeliHat
var heli_max_jump_height = 6
var heli_min_jump_height = 0.5
var heli_sec_jump_duration = 1.75


const hat_animations = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/eloq_idle.png"), Vector2(3,2)],
}

func _init(parent_reference).("HeliHat", parent_reference, hat_animations):
	if _parent.get_parent().controller_type == 0:
		_stored_max_jump_height = _parent.get_parent().max_jump_height
		_stored_min_jump_height = _parent.get_parent().min_jump_height
		_stored_sec_jump_duration = _parent.get_parent().sec_jump_duration

func clean_up():
	if _parent.get_parent().controller_type == 0:
		_parent.get_parent().max_jump_height = _stored_max_jump_height
		_parent.get_parent().min_jump_height = _stored_min_jump_height
		_parent.get_parent().sec_jump_duration = _stored_sec_jump_duration
		_parent.get_parent().recalculate_movement_settings()


func _physics_process(_delta):
	if Input.is_action_just_pressed("hat_action_1"):
		if GameState.getv("hat.isEnabled"):
			set_active(!_active)
			print("HeliHat: " , _active)


func set_active(mode):   # add animation
	if mode:
		_active = true
		_parent.get_parent().max_jump_height = heli_max_jump_height
		_parent.get_parent().min_jump_height = heli_min_jump_height
		_parent.get_parent().sec_jump_duration = heli_sec_jump_duration
	else:
		_active = false
		_parent.get_parent().max_jump_height = _stored_max_jump_height
		_parent.get_parent().min_jump_height = _stored_min_jump_height
		_parent.get_parent().sec_jump_duration = _stored_sec_jump_duration
	
	_parent.get_parent().recalculate_movement_settings()
