extends "GenericHat.gd"

const hat_animations = {
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/null_null.png"), Vector2(0,0)]
} # index[0]=path index[1]=frames in x and y axis

func _init(parent_reference).("NullHat", parent_reference, hat_animations):
	pass
