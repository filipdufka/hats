extends "GenericHat.gd"


var magnet_strength = 500
var max_magnet_range = 1000

const hat_animations = { # index[0]=path index[1]=frames in x and y axis
#	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/magnet_idle.png"), Vector2(2,2)],
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/magnet_spin.png"), Vector2(3,4)]
}

var _effect = preload("res://Scenes/Characters/Player/Hats/MagnetHat_effect.tscn").instance()
var _eff_push = null
var _eff_pull = null


func _init(parent_reference).("MagnetHat", parent_reference, hat_animations):
	_parent.add_child(_effect)
	_eff_pull = _effect.get_node("pull")
	_eff_push = _effect.get_node("push")

func clean_up():
	_effect.queue_free()


func _physics_process(_delta):
	var _mode = int(GameState.getv("hat.isEnabled")) * (Input.get_action_strength("hat_action_2") + -Input.get_action_strength("hat_action_1"))
	
	match int(_mode):
		0: #none
			_eff_push.emitting = false
			_eff_pull.emitting = false
		1: #pull
			_eff_push.emitting = false
			if _eff_pull.emitting == false:
				_eff_pull.emitting = true
			do_magnet_stuff(1)
			
		-1: #push
			_eff_pull.emitting = false
			if _eff_push.emitting == false:
				_eff_push.emitting = true
			do_magnet_stuff(-1)


func do_magnet_stuff(polarity=1):
	for magnetable in Globals.gimme_tree().get_nodes_in_group("magnetable"):
		var distance_to_magnetable = _parent.get_global_position().distance_to(magnetable.get_global_position())
		if distance_to_magnetable <= max_magnet_range or max_magnet_range == 0:
			var direction = (_parent.get_global_position() - magnetable.get_global_position()).normalized()
			var force = (Globals.magnet_C * magnet_strength * magnetable.magnet_strength) / pow(distance_to_magnetable, 2)
			if not Globals.is_negligible(force):
				magnetable.apply_central_impulse(force*direction*polarity)
