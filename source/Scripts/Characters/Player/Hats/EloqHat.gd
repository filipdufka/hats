extends "GenericHat.gd"


const hat_animations = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/eloq_idle.png"), Vector2(3,2)],
	"action": [preload("res://Resources/Visuals/Characters/Lapse/Hats/eloq_action.png"), Vector2(3,2)]
}

func _init(parent_reference).("EloqHat", parent_reference, hat_animations):
	pass
