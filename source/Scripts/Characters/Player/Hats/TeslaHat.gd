extends "GenericHat.gd"


var max_tesla_range = 500
var _last_nearest_usable_teslatable = null

const hat_animations = { # index[0]=path index[1]=frames in x and y axis
	"idle": [preload("res://Resources/Visuals/Characters/Lapse/Hats/tesla_idle.png"), Vector2(3,2)]
}

func _init(parent_reference).("TeslaHat", parent_reference, hat_animations):
	pass

func clean_up():
	if _last_nearest_usable_teslatable:
		_last_nearest_usable_teslatable.set_active(false)


func _physics_process(_delta):
	if GameState.getv("hat.isEnabled"):
		var nearest_usable_teslatable = _get_nearest_usable_teslatable()
		if nearest_usable_teslatable != _last_nearest_usable_teslatable:
			if _last_nearest_usable_teslatable:
				_last_nearest_usable_teslatable.set_active(false)
			if nearest_usable_teslatable:
				nearest_usable_teslatable.set_active(true)
		_last_nearest_usable_teslatable = nearest_usable_teslatable
		
		if Input.is_action_just_pressed("hat_action_1") and _last_nearest_usable_teslatable:
			_last_nearest_usable_teslatable.do_tesla_stuff()


func _get_nearest_usable_teslatable():
	var nearest_usable_teslatable = null
	for teslatable in Globals.gimme_tree().get_nodes_in_group("teslatable"):
		if not nearest_usable_teslatable or teslatable.global_position.distance_to(_parent.global_position) < nearest_usable_teslatable.global_position.distance_to(_parent.global_position):
			nearest_usable_teslatable = teslatable
	
	if nearest_usable_teslatable and (nearest_usable_teslatable.global_position.distance_to(_parent.global_position) < max_tesla_range or max_tesla_range == 0):
		return nearest_usable_teslatable
	else:
		return null
