extends KinematicBody2D
const controller_type = 1

#public and editor settings
export (bool) var start_flipped = false
export (float, 0.01, 100) var speed_tiles_per_sec = 3.6
export (float, 0, 1) var horizontal_lerp_weight = 0.6
export (float, 1, 1000) var vertical_speed_multiplier = 190
export (float, 1, 1000) var vertical_scale_down_multiplier = 1
#export (float) var ext_forces_damp = 0.1

#changing value of Collision Safe Margin may be required if physics collisions appears buggy
export (float) var physics_push_bounce_force = 75

#private variables basen on public settings
var _lerp_weight
var _speed
var _push_bounce_force

var _last_direction_y

#script global woking variables
var _velocity = Vector2(0,0)

enum{IDLE, WALK, RUN, AIR, JUMP, LAND, DANCE}
var state = IDLE
var floor_anle = 0
var direction = 0
var direction_y


var local_min_height = 0
var local_max_height = -290

#subnode cache
onready var GFX = $GFX


func _ready():
	GFX.flip_h = start_flipped
	
	recalculate_movement_settings()
	self.add_to_group("Characters")
	GFX.set_lapse_animation("idle")
	set_hat(GameState.getv("hat.active"))


func _enter_tree():
	Globals.player_ref = self

func _exit_tree():
	Globals.player_ref = null


func recalculate_movement_settings():
	_lerp_weight = 1 - horizontal_lerp_weight
	_speed =  speed_tiles_per_sec * Globals.tile_unit_size
	_push_bounce_force = physics_push_bounce_force if physics_push_bounce_force > 0 else 0


func _physics_process(delta):
	direction = int(GameState.getv("player.canMove")) * (-Input.get_action_strength("move_left") + Input.get_action_strength("move_right"))
	direction_y = int(GameState.getv("player.canMove")) * (-Input.get_action_strength("move_up") + Input.get_action_strength("move_down"))
	_velocity.x = lerp(_velocity.x, _speed * direction, _lerp_weight)
	_velocity.y = direction_y * vertical_speed_multiplier
	
#	#flip sprite
	if direction == 1:
		GFX.flip_h = true
	elif direction == -1:
		GFX.flip_h = false
	elif direction_y != _last_direction_y and direction_y != 0:
		GFX.flip_h = !GFX.flip_h
	
	_last_direction_y = direction_y
	
	#Garbaj
	match state:
		IDLE:
			GFX.set_lapse_animation("idle")
			if direction or direction_y:
				state = WALK
		WALK:
			GFX.set_lapse_animation("walk")
			if not direction and not direction_y:
				state = IDLE
		RUN:
			GFX.set_lapse_animation("run")
			if not direction or direction_y:
				state = IDLE
		DANCE:
			GFX.set_lapse_animation("dance")
			if direction or direction_y:
				state = IDLE
	
	#moving
	var collision = move_and_collide(_velocity * delta, false)
	if collision:
		_velocity = _velocity.slide(collision.normal)
		if collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)
		var remainder = collision.remainder.slide(collision.normal)
		collision = move_and_collide(remainder)
		#These two lines are not critical, but make physics collisions a bit smoother
		if collision and collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)
	
	global_position.y = clamp(global_position.y, local_max_height, local_min_height)


func set_hat(new_hat="NullHat", force=false):
	if new_hat == null:
		new_hat = "NullHat"
	GFX.set_hat(new_hat, force)



func _on_GFX_animation_finished():
	#dirty fix for animation stutter
	if state == JUMP:
		GFX.set_lapse_animation("air")
		state = AIR
	elif state == LAND:
		GFX.set_lapse_animation("idle")
		state = IDLE
