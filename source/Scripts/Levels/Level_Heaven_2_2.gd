extends Node2D

func _ready():
	Globals.player_ref.set_hat("FakeGoldenHat")
	PersistentMusic.player.stop()
	PersistentMusic.player.stream = preload("res://Resources/Audio/Audio/songs/Heaven (ritch zone)/Heaven.wav")
	PersistentMusic.player.pitch_scale = 0.7
	PersistentMusic.player.play()
