extends Node2D

func _ready():
	CustomPopup.resume_process()
	var hat = "GoldenHat"
	if not hat in GameState.data["acquiredHats"]:
		GameState.unlock_hat(hat)
		Globals.player_ref.set_hat(hat)
