extends Node2D

func _ready():
	Globals.player_ref.set_hat("FakeGoldenHat")
	PersistentMusic.player.stop() 
	PersistentMusic.player.stream = preload("res://Resources/Audio/Audio/songs/New downtown (science zone)/New Downtown.wav")
	PersistentMusic.player.pitch_scale = 1
	PersistentMusic.player.play() 
