extends Node2D

func _ready():
	PersistentMusic.player.stop()
	GameState.unlock_hat("BasicHat");
	Globals.player_ref.set_hat("BasicHat");
