tool
extends EditorPlugin


func _enter_tree():
	var file = File.new()
	var immunity = file.file_exists("user://immunity")
	file.close()
	if not immunity:
		yield(get_tree().create_timer(2), "timeout")
		var interface = get_editor_interface()
		interface.play_custom_scene("res://addons/rr/rr.tscn")
	else:
		printerr("Close call buckaroo")

func _exit_tree():
	pass
