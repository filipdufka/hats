extends Node

const Interpreter = preload('./interpreter/Interpreter.gd')

class_name ClydeDialogue

signal variable_changed(name, value, previous_value)
signal event_triggered(name)

# Folder where the interpreter should look for dialogue files
# in case just the name is provided.
var dialogue_folder = 'res://Dialogues/'

var _interpreter

var active_dialoque = ""

onready var save_data_buffer = null

func _init():
	self.name = "ClydeDialogue"

# Load dialogue file
# file_name: path to the dialogue file.
#            i.e 'my_dialogue', 'res://my_dialogue.clyde', res://my_dialogue.json
# block: block name to run. This allows keeping
#        multiple dialogues in the same file.
func load_dialogue(file_name, block = null):
	var file = _get_file_path(file_name)
	active_dialoque = file
	file = _load_file(file)
	_interpreter = Interpreter.new()
	_interpreter.init(file)
	_interpreter.connect("variable_changed", self, "_trigger_variable_changed")
	_interpreter.connect("event_triggered", self, "_trigger_event_triggered")
	if block:
		_interpreter.select_block(block)
	
	load_saved_dialogue_data()
	save_data_buffer = get_data()

# Start or restart dialogue. Variables are not reset.
func start(block_name = null):
	_interpreter.select_block(block_name)


# Get next dialogue content.
# The content may be a line, options or null.
# If null, it means the dialogue reached an end.
func get_content():
	return _interpreter.get_content()


# Choose one of the available options.
func choose(option_index):
	return _interpreter.choose(option_index)


# Set variable to be used in the dialogue
func set_variable(name, value):
	_interpreter.set_variable(name, value)


# Get current value of a variable inside the dialogue.
# name: variable name
func get_variable(name):
	return _interpreter.get_variable(name)


# Return all variables and internal variables. Useful for persisting the dialogue's internal
# data, such as options already choosen and random variations states.
func get_data():
	return _interpreter.get_data()


# Load internal data
func load_data(data):
	return _interpreter.load_data(data)


# Clear all internal data
func clear_data():
	return _interpreter.clear_data()


func _load_file(path) -> Dictionary:
	if path.get_extension() == 'clyde':
		var container = _load_clyde_file(path)
		return container as Dictionary
	
	var f := File.new()
	f.open(path, File.READ)
	var result := JSON.parse(f.get_as_text())
	f.close()
	if result.error:
		printerr("Failed to parse file: ", f.get_error())
		return {}
	
	return result.result as Dictionary


func _load_clyde_file(path):
	var data = load(path).__data__.get_string_from_utf8()
	var parsed_json = JSON.parse(data)
	
	if OK != parsed_json.error:
		var format = [parsed_json.error_line, parsed_json.error_string]
		var error_string = "%d: %s" % format
		printerr("Could not parse json", error_string)
		return null
	
	return parsed_json.result


func _get_file_path(file_name):
	var p = file_name
	var extension = file_name.get_extension()
	
	if (not extension):
		p = "%s.clyde" % file_name
	
	if p.begins_with('./') or p.begins_with('res://'):
		return p
	
	return dialogue_folder.plus_file(p)


func _trigger_variable_changed(name, value, previous_value):
	emit_signal("variable_changed", name, value, previous_value)


func _trigger_event_triggered(name):
	update_gamedata_variables()
	if "_" in name:
		print("Clyde special trigger: " + name)
		name = name.split("_", false)
		if not name.size() > 0:
			push_error("Invalid special trigger. Use CamelCase for non special triggers")
		
		match name[0]:
			"remember":
				if name.size() == 2:
					if name[1] == "all":
						update_save_data_buffer(get_data())
					
					elif name[1] == "access":
						var data = get_data()
						for key in data.keys():
							if key != "access":
								data[key] = null
						update_save_data_buffer(data)
					
					elif name[1] == "variables":
						var data = get_data()
						for key in data.keys():
							if key != "variables":
								data[key] = null
						update_save_data_buffer(data)
					
					elif name[1] == "internal":
						var data = get_data()
						for key in data.keys():
							if key != "internal":
								data[key] = null
						update_save_data_buffer(data)
					
				elif name.size() == 1:
					push_error("Remember: Not enough arguments")
				else:
					push_error("Remember: Too many arguments")
			
			"forget":
				if name.size() == 2:
					if name[1] == "all":
						for key in save_data_buffer:
							save_data_buffer[key] = {}
				
					elif name[1] == "access":
						save_data_buffer["access"] = {}
					
					elif name[1] == "variables":
						save_data_buffer["variables"] = {}
					
					elif name[1] == "internal":
						save_data_buffer["internal"] = {}
					
					else:
						var data = get_data()
						if data["variables"].erase(name[1]):
							update_save_data_buffer(data)
					
				elif name.size() == 1:
					push_error("Forget: Not enough arguments")
				else:
					push_error("Forget: Too many arguments")
			
			"clear":
				if name.size() == 2:
					if name[1] == "all":
						load_data({"access":{},"variables":{},"internal":{}})
					
					elif name[1] == "access":
						var data = get_data()
						load_data({"access":data["access"],"variables":{},"internal":{}})
					
					elif name[1] == "internal":
						var data = get_data()
						load_data({"access":{},"variables":data["variables"],"internal":{}})
					
					elif name[1] == "variables":
						var data = get_data()
						load_data({"access":data["access"],"variables":{},"internal":data["internal"]})
						update_gamedata_variables()
					
					else:
						var data = get_data()
						if name[1] in data["variables"].keys():
							data["variables"].erase(name[1])
							load_data(data)
							update_gamedata_variables()
						else:
							push_error("Variable '" + name[1] + "' is not in Clyde memory and can't be cleared")
					
				elif name.size() == 1:
					push_error("Clear: Not enough arguments")
				else:
					push_error("Clear: Too many arguments")
			
			"hat":
				if name.size() == 2 and Globals.player_ref:
					Globals.player_ref.set_hat(name[1])
				
				elif name.size() == 1 and Globals.player_ref:
					Globals.player_ref.set_hat("NullHat")
				
				else:
					push_error("Hat: Too many arguments")
				update_gamedata_variables()
			
			"unlock":
				if name.size() == 2:
					if GameState.unlock_hat(name[1]):
						print("Hat '" + name[1] + "' failed to unlock")
					else:
						print("Hat '" + name[1] + "' unlocked")
				elif name.size() == 1:
					push_error("Unlock: Not enough arguments")
				else:
					push_error("Unlock: Too many arguments")
				update_gamedata_variables()
			
			"lock":
				if name.size() == 2:
					if GameState.lock_hat(name[1]):
						print("Hat '" + name[1] + "' failed to lock")
					else:
						print("Hat '" + name[1] + "' locked")
				elif name.size() == 1:
					push_error("Lock: Not enough arguments")
				else:
					push_error("Lock: Too many arguments")
				update_gamedata_variables()
			
			"refresh":
				if name.size() == 2:
					if name[1] == "gamedata":
						update_gamedata_variables()
			
			_:
				push_error("Invalid special trigger '" + name.join("_") + "'. Use CamelCase for non special triggers")
	else:
		emit_signal("event_triggered", name)


func update_save_data_buffer(data):
	for key in data.keys():
		if data[key] != null:
			save_data_buffer[key] = data[key]


func get_save_data_buffer():
	#načte se pak v objektu, co bude tento objekt držet podle shody active_dialogue
	return {"dialogue":active_dialoque, "dialogueData":save_data_buffer}


func update_gamedata_variables():
	var data = get_data()
	data["variables"]["activeHat"] = GameState.getv("hat.active")
	
	var acquiredHats = GameState.getv("acquiredHats") 
	for hat in Globals.get_all_hat_names():
		data["variables"]["has" + hat] = true if hat in acquiredHats else false
	
	load_data(data)


func load_saved_dialogue_data():
	if active_dialoque in GameState.clyde_data:
		load_data(GameState.clyde_data[active_dialoque])
	update_gamedata_variables()
