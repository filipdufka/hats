extends Reference
class_name GDShell, "icon.png"

#Debug mode for GDShell. If false, some commands are forbidden (for example for release)
#var debug : bool = false

var _command_pool : Object = preload("res://addons/TGT_GDShell/GDShellCommands.gd").new()


func execute_raw(command: String) -> String:
	var parsed_command = parse(command)
	if parsed_command["command"]:
		return execute_parsed(parsed_command)
	else: #err=1 -> empty command -> do nothing
		return ""


func execute_parsed(command: Dictionary) -> String:
	if _command_pool.has_method(command["command"]) and command["command"][0] != "_":
		if (not command["command"] in _command_pool._only_debug_commands) or Globals.debug:
			return _command_pool.call(command["command"], command["args"], command["inputs"])
		else:
			return "'" + command["command"] + "' is available only in debug mode"
	elif command["command"] == "clear" or command["command"] == "cls":
		return "cls"
	else:
		return "'" + command["command"] + "' is not recognised as a command"


#err: 0-OK 1-empty
func parse(command: String) -> Dictionary:
	var out = {"command":"", "args":[], "inputs":[]}
	var splitted_command = _split_safe(command)
	
	if splitted_command:
		out["command"] = splitted_command[0]
		
		if splitted_command.size() > 1:
			for i in range(1, splitted_command.size()):
				if splitted_command[i][0] == "-":
					out["args"].push_back(splitted_command[i].substr(1,-1))
				else:
					out["inputs"].push_back(splitted_command[i])
	
	return out


func _split_safe(to_split: String) -> Array:
	var out = [""]
	var can_split = true
	for character in to_split:
		if character == " " and can_split:
			out.append("")
		elif character == '"' or character == "'":
			can_split = !can_split
		else:
			out[-1] += character
	
	while out.has(""):
		out.erase("")
	
	return out
