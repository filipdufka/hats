extends Reference

var _only_debug_commands = ["scene", "debug", "hat"]
const name = "GDShell"

var _fps_scene = preload("res://Scenes/UI/Canvas/FPS.tscn").instance()
func _init():
	_fps_scene.get_node("FPS").visible = false
	Globals.add_child(_fps_scene)


func help(args: Array, inputs:Array) -> String:
	var out = _help_help()
	if args:
		out = ""
		match args[0]:
			"l":
				for arg in get_script().get_script_method_list():
					if arg["name"][0] != "_":
						out += arg["name"] + "\n"
			"a":
				for arg in get_script().get_script_method_list():
					if arg["name"][0] != "_":
						if not arg["name"] in _only_debug_commands:
							out += arg["name"] + "\n"
			"d":
				for arg in get_script().get_script_method_list():
					if arg["name"][0] != "_":
						if arg["name"] in _only_debug_commands:
							out += arg["name"] + "\n"
			_:
				return "Invalid argument\n"
	elif inputs:
		if has_method("_"+inputs[0]+"_help"):
			out = call("_"+inputs[0]+"_help")
		else:
			out = "Cannot get help for command '" + inputs[0] + "'"
	return out


func _help_help():
	return "Provides help information for GDShell commands.\n\nhelp [command] - command=command you wish to get help for\nOptions:   -l - Shows all commands\n                -a - Shows all available commands\n                -d - Shows all debug commands\n"

# Dočasné řešení
# Ukradeno z res://Scripts/Singletons/AppData.gd
func _directory_content(dir_name=""):
	var dir = Directory.new()
	dir.open(dir_name)
	if dir.list_dir_begin(true) == OK:
		var content = []
		while true:
			var file = dir.get_next()
			if file != "":
				content.append(file)
			else:
				break
		dir.list_dir_end()
		return content
	return null


func scene(args: Array, inputs: Array) -> String:
	if args:
		match args[0]:
			"list":
				var out = ""
				for file in _directory_content("res://Scenes/Levels"):
					file = file.replace(".tscn", "")
					out += file + "\n"
				return out
			"load":
				if inputs and inputs[0]:
					var path = inputs[0]
					if not path.begins_with("res://"):
						path = "res://Scenes/Levels/" + path
					if not path.ends_with(".tscn"):
						path += ".tscn"
					if not ResourceLoader.exists(path):
						return "Requested resource not found!"
					SceneChanger.change_scene(path)
					return "Changing scene to '" + path + "' ..."
			"reload":
				var path = Globals.gimme_tree().get_current_scene().get_filename()
				Globals.gimme_tree().reload_current_scene()
				return "Reloading current scene '" + path + "' ..."
			"_":
				return "Invalid option!"
	else:
		if inputs and inputs[0]:
			var path = inputs[0]
			if not path.begins_with("res://"):
				path = "res://Scenes/Levels/" + path
			if not path.ends_with(".tscn"):
				path += ".tscn"
			if not ResourceLoader.exists(path):
				return "Requested resource not found!"
			SceneChanger.change_scene(path)
			return "Changing scene to '" + path + "' ..."
	return "Invalid arguments!"


func _scene_help():
	return "Provides scene manipulation.\n\nscene [option]\nOptions:  -list - Shows all scenes\n                -load <path> - Loads specified scene (default)\n                -reload - Reloads current scene\n"


func hat (args: Array, inputs: Array) -> String:
	if args:
		match args[0]:
			"list":
				if Globals.player_ref:
					var out = ""
					for hat in Globals.player_ref.GFX.hats.keys():
						out += hat + "\n"
					return out
				else:
					return "Can't list hats right now"
			"acquired":
				var acquired = GameState.getv("acquiredHats")
				var out = ""
				for hat in acquired:
					out += hat + "\n"
				return out
			"force":
				if inputs:
					if Globals.player_ref:
						Globals.player_ref.set_hat(inputs[0], true)
						return "Force setting hat to " + inputs[0]
					else:
						return "Cannot change hat right now"
				else:
					return "Not enought arguments. Use [color=#0000FF]help hat[/color] for more information"
			"current":
				return GameState.getv("hat.active")
			"lock":
				if inputs:
					GameState.lock_hat(inputs[0])
					return "Hat " + inputs[0] + " locked\n"
				else:
					return "Hat must be specified\n"
			"unlock":
				if inputs:
					GameState.unlock_hat(inputs[0])
					return "Hat " + inputs[0] + " unlocked\n"
				else:
					return "Hat must be specified\n"
			"set":
				if inputs:
					if Globals.player_ref:
						Globals.player_ref.set_hat(inputs[0], false)
						return "Hat set to " + inputs[0] + "\n"
					else:
						return "Cannot change hat right now\n"
				else:
					return "Invalid arguments. Use [color=#0000FF]help hat[/color] for more information"
				
			_:
				return "Invalid arguments. Use [color=#0000FF]help hat[/color] for more information"
		
	elif inputs:
		if Globals.player_ref:
			Globals.player_ref.set_hat(inputs[0], false)
			return "Hat set to " + inputs[0] + "\n"
		else:
			return "Cannot change hat right now"
	else:
		return "Not enought arguments. Use [color=#0000FF]help hat[/color] for more information"


func _hat_help():
	return "Provides hat utility.\n\nhat [option] <hat>\nOptions:  -list - Shows all hats\n                -acquired - Shows acquired hats\n                -lock <hat> - Locks specified hat\n                -unlock <hat> - Unlocks specified hat\n                -current - Shows current hat\n                -force <hat> - Force sets specified hat\n                -set <hat> - Sets specified hat\n                <hat> - Sets specified hat"


func quit(args: Array, inputs: Array) -> String:
	if args:
		if args[0] == "force":
			Globals._quit(self)
			return "Force quitting"
		elif args[0] == "safe":
			Globals.quit(self)
			return "Quit request sent"
		else:
			return "Invalid arguments. Use [color=#0000FF]help quit[/color] for more information"
	else:
		Globals.quit(self)
		return "Quit request sent"

func _quit_help():
	return "Quits the app.\n\nquit [option]\nOptions:  -force - Force quits the app (may cause memory leak)\n                -safe - Safely exits app (default)\n"


func window(args: Array, inputs: Array) -> String:
	if args:
		match args[0]:
			"fullscreen":
				Window.fullscreen()
				return "Changing screen mode to Fullscreen\n"
			"windowed":
				Window.windowed()
				return "Changing screen mode to Windowed\n"
			"borderless":
				Window.windowed_borderless()
				return "Changing screen mode to Borderless\n"
			_:
				return "Invalid arguments. Use [color=#0000FF]help screen[/color] for more information"
	else:
		return "Not enough arguments. Use [color=#0000FF]help screen[/color] for more information"

func _window_help():
	return "Changes screen settings\n\nscreen [option]\nOptions:  -fullscreen - Sets window to fullscreen mode\n                -windowed - Sets window to fullscreen mode\n                -borderless - Sets window to borderless mode\n"

func debug(args: Array, inputs: Array) -> String:
	if args:
		match args[0]:
			"list":
				var out = ""
				for file in _directory_content("res://debug"):
					file = file.replace(".tscn", "")
					out += file + "\n"
				return out
			"load":
				if inputs and inputs[0]:
					var path = inputs[0]
					if not path.begins_with("res://"):
						path = "res://debug/" + path
					if not path.ends_with(".tscn"):
						path += ".tscn"
					if not ResourceLoader.exists(path):
						return "Requested resource not found!"
					SceneChanger.change_scene(path)
					return "Changing scene to '" + path + "' ..."
			"reload":
				var path = Globals.gimme_tree().get_current_scene().get_filename()
				Globals.gimme_tree().reload_current_scene()
				return "Reloading current scene '" + path + "' ..."
			"_":
				return "Invalid option!"
	else:
		if inputs and inputs[0]:
			var path = inputs[0]
			if not path.begins_with("res://"):
				path = "res://debug/" + path
			if not path.ends_with(".tscn"):
				path += ".tscn"
			if not ResourceLoader.exists(path):
				return "Requested resource not found!"
			SceneChanger.change_scene(path)
			return "Changing scene to '" + path + "' ..."
	return "Invalid arguments!"

func _debug_help():
	return "Provides debug scene manipulation (scene command for debug scenes)\n\ndebug [option]\nOptions:  -list - Shows all scenes\n                -load <path> - Loads specified scene (default)\n                -reload - Reloads current scene\n"


func dmode(args: Array, inputs: Array) -> String:
	if args:
		if args[0] == "true":
			Globals.debug = true
			Config.setv("game.debug", true)
			return "Debug mode on"
		elif args[0] == "false":
			Globals.debug = false
			Config.setv("game.debug", false)
			return "Debug mode off"
		elif args[0] == "mode":
			return "Debug mode is " + ("on" if Globals.debug else "off")
		else:
			return "Invalid arguments"
	else:
		return "Not enough arguments"

func _dmode_help():
	return "Sets debug mode\n\ndMode [option]\nOptions:  -true  - Enables debug mode\n                -false - Disables debug mode\n                -mode  - Show current debug mode\n"

func fps(args: Array, inputs: Array) -> String:
	_fps_scene.get_node("FPS").visible = not _fps_scene.get_node("FPS").visible
	return "FPS: " + str(_fps_scene.get_node("FPS").visible) 

func _fps_help():
	return "Toggles fps overlay"
