extends Node

var console_activator_action : String = "console_toggle" 
var use_console_activator_action : bool = true
#var enabled : bool = true setget set_enabled
#var debug : bool = true setget set_debug

var shell_window : Object = preload("res://addons/TGT_GDShell/GDShellWindow.tscn").instance()


func _init() -> void:
	set_pause_mode(2)
	var canvas_layer = CanvasLayer.new()
	canvas_layer.set_layer(99)
	canvas_layer.add_child(shell_window)
	add_child(canvas_layer)


func _input(event) -> void:
	if event.is_action_pressed(console_activator_action) and use_console_activator_action:
		toggle_console()


func enable_console() -> void:
	shell_window.enable()


func disable_console() -> void:
	shell_window.disable()


func toggle_console() -> void:
	if shell_window.is_visible():
		disable_console()
	else:
		enable_console()
