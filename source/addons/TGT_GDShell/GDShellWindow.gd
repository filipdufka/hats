extends WindowDialog

#If true, the window is forbidden to open
var window_forbidden : bool = false

var history : Array = []
var historyUp : String = "console_history_up" 
var historyDown : String = "console_history_down" 
var _history_pointer : int = 0

var gdsh : GDShell = GDShell.new()
#var GDShell : Object = preload("res://addons/TGT_GDShell/GDShell.gd").new()

signal window_enabled
signal window_disabled
signal window_changed(enabled)
signal window_input(input)


func _ready() -> void:
	disable_ShellDisplay() #TODO: Implement ShellDisplay utility


#GDShellWindow
func is_enabled() -> bool:
	return visible


func enable() -> void:
	if not window_forbidden:
		show()
		$VBoxContainer/ShellInput.call_deferred("grab_focus")
	else:
		push_warning(str(get_script().get_path()) + ":   window_forbidden is set to true  GDShellWindow can't open by design.")


func disable() -> void:
	hide()


func toggle() -> void:
	if is_enabled():
		disable()
	else:
		enable()


func _on_GDShellWindow_visibility_changed() -> void:
	if is_enabled():
		#stop movement quick fix
		GameState.setv("player.canMove", false)
		GameState.setv("hat.isEnabled", false)
		
		emit_signal("window_enabled")
		emit_signal("window_changed", true)
	else:
		#stop movement quick fix
		GameState.setv("player.canMove", true)
		GameState.setv("hat.isEnabled", true)
		
		emit_signal("window_disabled")
		emit_signal("window_changed", false)


#ShellOutput
func enable_ShellOutput() -> void:
	$VBoxContainer/HSplitContainer/ShellOutput.visible = true


func disable_ShellOutput() -> void:
	$VBoxContainer/HSplitContainer/ShellOutput.visible = false


func toggle_ShellOutput() -> void:
	if $VBoxContainer/HSplitContainer/ShellOutput.visible:
		disable_ShellOutput()
	else:
		enable_ShellOutput()


#ShellDisplay
func enable_ShellDisplay() -> void:
	$VBoxContainer/HSplitContainer/ShellDisplay.visible = true


func disable_ShellDisplay() -> void:
	$VBoxContainer/HSplitContainer/ShellDisplay.visible = false


func toggle_ShellDisplay() -> void:
	if $VBoxContainer/HSplitContainer/ShellDisplay.visible:
		disable_ShellDisplay()
	else:
		enable_ShellDisplay()


#Called when input is given to LineEdit ShellInput
func _on_ShellInput_text_entered(input) -> void:
	emit_signal("window_input", input)
	windowOutln("user>"+input)
	execute(input)
	$VBoxContainer/ShellInput.text = ""
	_history_pointer = 0
	#Ensures there isn't the same input right behind
	if not history.size() or history[0] != input:
		history.push_front(input)


#Just for a check for up and down history buttons
func _input(event) -> void:
	#History
	if $VBoxContainer/ShellInput.has_focus():
		if event.is_action_pressed(historyUp):
			$VBoxContainer/ShellInput.call_deferred("grab_focus")
			$VBoxContainer/ShellInput.text = _get_input_from_history(1)
			get_tree().set_input_as_handled()
			$VBoxContainer/ShellInput.set_cursor_position($VBoxContainer/ShellInput.text.length())
		elif event.is_action_pressed(historyDown):
			$VBoxContainer/ShellInput.call_deferred("grab_focus")
			$VBoxContainer/ShellInput.text = _get_input_from_history(-1)
			get_tree().set_input_as_handled()
			$VBoxContainer/ShellInput.set_cursor_position($VBoxContainer/ShellInput.text.length())


func _get_input_from_history(move: int) -> String:
	var out = history[_history_pointer] if history.size() else ""
	_history_pointer = clamp(_history_pointer+move, 0, history.size()-1)
	return out


#Print
func windowOut(to_print) -> void:
	$VBoxContainer/HSplitContainer/ShellOutput.bbcode_text += str(to_print)


#Print on new line
func windowOutln(to_print) -> void:
	$VBoxContainer/HSplitContainer/ShellOutput.bbcode_text += "\n" + str(to_print)


func windowCls() -> void:
	$VBoxContainer/HSplitContainer/ShellOutput.bbcode_text = ""


func execute(command: String) -> void: 
	var out = gdsh.execute_raw(command)
	if out != "cls":
		windowOutln(out)
	else:
		windowCls()
