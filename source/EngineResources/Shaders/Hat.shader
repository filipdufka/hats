shader_type canvas_item;

uniform int hat_frame;
uniform int hat_animation_x;
uniform int hat_animation_y;
uniform sampler2D hat_animation;
uniform sampler2D hat_animation_pos : hint_albedo;


void fragment() {
	vec2 cr = vec2(float(hat_animation_x),float(hat_animation_y));
	float total_frames = cr.x * cr.y;
	float frame = mod(float(hat_frame),total_frames);

	vec2 frame_pos = vec2(mod(float(frame),float(hat_animation_y)),floor(float(frame)/float(hat_animation_x)));
	vec2 uv_pos = frame_pos / cr;

	vec4 origin = texture(TEXTURE,UV);
	vec4 hat_placement = texture(hat_animation_pos, UV);
	if(hat_placement.a > 0.1){
		vec4 hat_sample = texture(hat_animation, hat_placement.xy/cr + uv_pos);
		COLOR = mix(origin,hat_sample, hat_sample.a);
	}else{
		COLOR = origin;
	}
}