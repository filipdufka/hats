import os, shutil, sys

#Some useful tools for you
#Use this, if you have Python installed or you want to compile this garbage on our own
user_file_system_path = os.getenv("APPDATA") + "\\HATS\\"
desktop_out_dir = os.path.join(os.environ["USERPROFILE"]) +  "\\Desktop\\dev_tools"

RESET = lambda x: "\033[0m" + str(x)

UNDERLINE = lambda x: "\033[4m" + str(x)

DEFAULT = lambda x: "\033[39m" + str(x)
BLACK =lambda x: "\033[30m" + str(x)
WHITE = lambda x: "\033[97m" + str(x)
RED = lambda x: "\033[31m" + str(x)
GREEN = lambda x: "\033[32m" + str(x)
YELLOW = lambda x: "\033[33m" + str(x)
BLUE = lambda x: "\033[34m" + str(x)
MAGENTA = lambda x: "\033[35m" + str(x)
CYAN = lambda x: "\033[36m" + str(x)
LIGHTGRAY = lambda x: "\033[37m" + str(x)
DARKGRAY = lambda x: "\033[90m" + str(x)
LIGHTRED = lambda x: "\033[91m" + str(x)
LIGHTGREEN = lambda x: "\033[92m" + str(x)
LIGHTYELLOW = lambda x: "\033[93m" + str(x)
LIGHTBLUE = lambda x: "\033[94m" + str(x)
LIGHTMAGENTA = lambda x: "\033[95m" + str(x)
LIGHTCYAN = lambda x: "\033[96m" + str(x)

BG_DEFAULT = lambda x: "\033[49m" + str(x)
BG_BLACK = lambda x: "\033[40m" + str(x)
BG_WHITE = lambda x: "\033[107m" + str(x)
BG_RED = lambda x: "\033[41m" + str(x)
BG_GREEN = lambda x: "\033[42m" + str(x)
BG_YELLOW = lambda x: "\033[43m" + str(x)
BG_BLUE = lambda x: "\033[44m" + str(x)
BG_MAGENTA = lambda x: "\033[45m" + str(x)
BG_CYAN = lambda x: "\033[46m" + str(x)
BG_LIGHTGRAY = lambda x: "\033[47m" + str(x)
BG_DARKGRAY = lambda x: "\033[100m" + str(x)
BG_LIGHTRED = lambda x: "\033[101m" + str(x)
BG_LIGHTGREEN = lambda x: "\033[102m" + str(x)
BG_LIGHTYELLOW = lambda x: "\033[103m" + str(x)
BG_LIGHTBLUE = lambda x: "\033[104m" + str(x)
BG_LIGHTMAGENTA = lambda x: "\033[105m" + str(x)
BG_LIGHTCYAN = lambda x: "\033[106m" + str(x)


def get_logs():
    path = user_file_system_path + "logs\\"
    logs_in_path = os.listdir(path)
    for i in range(len(logs_in_path)):
        if logs_in_path[i].split(".")[-1] == "log":
            f = open(path+logs_in_path[i], "r")
            logs_in_path[i] = [logs_in_path[i], f.read()]
            f.close()
    return logs_in_path


def format_logs(logs):
    single_log = "Last logs\n"
    for log in logs:
        single_log += "\n*" + log[0] + "*\n=====LOG START=====\n" + log[1] + "=====LOG END=====\n\n" 
    return single_log[:-2]


def get_logs_to_dir():
    log = format_logs(get_logs())
    f = open(desktop_out_dir+"\\godot.logs", "w")
    f.write(log)
    f.close()


def wipe_local_files(mode=0, option=0):
    files = os.listdir(user_file_system_path)
    if mode == 0: #all except logs
        for f in os.listdir(user_file_system_path):
            if f != "logs":
                path = os.path.join(user_file_system_path, f)                
                if os.path.isdir(path):
                    shutil.rmtree(path)
                elif os.path.isfile(path):
                    os.remove(path)

    elif mode == 1: #only saves
        if option == 0: #also delete save directory
            shutil.rmtree(os.path.join(user_file_system_path, "saves"))
        elif option == 1: #only deletes directory contents
            for f in os.listdir(user_file_system_path+"saves"):
                path = os.path.join(user_file_system_path+"saves", f)               
                if os.path.isdir(path):
                    shutil.rmtree(path)
                elif os.path.isfile(path):
                    os.remove(path)


def get_saves_to_dir():
    path = user_file_system_path + "saves\\"
    saves_in_path = os.listdir(path)
    for save in saves_in_path:
        if os.path.isfile(path+save):
            shutil.copyfile(path+save, desktop_out_dir+"\\"+save)


if __name__ == "__main__":
    if sys.platform.lower() == "win32":
        os.system('color')
    if not os.path.exists(user_file_system_path):
        print(RED("Filesystem not found")+RESET(""))
        os.system("pause")
        sys.exit()
    if not os.path.exists(desktop_out_dir):
        print("Creating desktop output directory...")
        os.makedirs(desktop_out_dir)

    print("Dev tools\n\nUse help for help")
    while True:
        inp = input("\n"+LIGHTBLUE("> ")+RESET(""))

        if inp == "help":
            print("Commands:\n\nget logs\nget saves\nclear all\nclear saves\nexit")
        elif inp == "get logs":
            get_logs_to_dir()
            print("Logs saved to desktop")
        elif inp == "get saves":
            get_saves_to_dir()
            print("Saves saved to desktop")
        elif inp == "clear all":
            wipe_local_files()
            print("All local data has been deleted")
        elif inp == "clear saves":
            option = input("Would you also like to delete the 'saves' directory? [Y/n] ")
            if option.lower() == "y":
                wipe_local_files(1, 0)
                print("Save file deleted")
            else:
                wipe_local_files(1, 1)
                print("All saves deleted")
        elif inp == "exit":
            sys.exit()
        else:
            print(RED("Unknown command") + RESET(""))